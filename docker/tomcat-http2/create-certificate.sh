openssl req -newkey 2048 -nodes -keyout conf/tomcat.key -x509 -days 365 -out conf/tomcat.crt -config conf/openssl.cnf
openssl pkcs12 -inkey conf/tomcat.key -in conf/tomcat.crt -export -out conf/tomcat.pfx -passout pass:azerty
