xmlstarlet c14n --without-comments conf/server.xml > conf/server.xml.tmp
xmlstarlet ed --inplace conf/server.xml.tmp

# Add APR connector
xmlstarlet ed --inplace  \
              --append /Server/Service/Connector --type elem --name "Connector"    \
              --var connector_node '$prev' \
              --insert '$connector_node' --type attr --name "port" --value "8442" \
              --insert '$connector_node' --type attr --name "SSLEnabled" --value "true" \
              --insert '$connector_node' --type attr --name "protocol" --value "org.apache.coyote.http11.Http11AprProtocol" \
              --subnode '$connector_node' --type elem --name "UpgradeProtocol"    \
              --var upgrade_node '$prev' \
              --insert '$upgrade_node' --type attr --name "className" --value "org.apache.coyote.http2.Http2Protocol" \
              --subnode '$connector_node' --type elem --name "SSLHostConfig"    \
              --var host_node '$prev' \
              --insert '$host_node' --type attr --name "protocols" --value "TLSv1.2,TLSv1.3" \
              --subnode '$host_node' --type elem --name "Certificate"    \
              --var certificate_node '$prev' \
              --insert '$certificate_node' --type attr --name "certificateFile" --value "conf/tomcat.crt"   \
              --insert '$certificate_node' --type attr --name "certificateKeyFile" --value "conf/tomcat.key"   \
              conf/server.xml.tmp

# Add JSSE connector
xmlstarlet ed --inplace     \
              --append /Server/Service/Connector[@port="8442"] --type elem --name "Connector"    \
              --var connector_node '$prev' \
              --insert '$connector_node' --type attr --name "port" --value "8443" \
              --insert '$connector_node' --type attr --name "SSLEnabled" --value "true" \
              --insert '$connector_node' --type attr --name "protocol" --value "org.apache.coyote.http11.Http11NioProtocol" \
              --insert '$connector_node' --type attr --name "sslImplementationName" --value "org.apache.tomcat.util.net.jsse.JSSEImplementation" \
              --subnode '$connector_node' --type elem --name "UpgradeProtocol"    \
              --var upgrade_node '$prev' \
              --insert '$upgrade_node' --type attr --name "className" --value "org.apache.coyote.http2.Http2Protocol" \
              --subnode '$connector_node' --type elem --name "SSLHostConfig"    \
              --var host_node '$prev' \
              --insert '$host_node' --type attr --name "protocols" --value "TLSv1.2,TLSv1.3" \
              --subnode '$host_node' --type elem --name "Certificate"    \
              --var certificate_node '$prev' \
              --insert '$certificate_node' --type attr --name "certificateKeystoreFile" --value "conf/tomcat.pfx"   \
              --insert '$certificate_node' --type attr --name "certificateKeystorePassword" --value "azerty"   \
              conf/server.xml.tmp

# Add OpenSSL connector
xmlstarlet ed --inplace     \
              --append /Server/Service/Connector[@port="8443"] --type elem --name "Connector"    \
              --var connector_node '$prev' \
              --insert '$connector_node' --type attr --name "port" --value "8444" \
              --insert '$connector_node' --type attr --name "SSLEnabled" --value "true" \
              --insert '$connector_node' --type attr --name "protocol" --value "org.apache.coyote.http11.Http11NioProtocol" \
              --insert '$connector_node' --type attr --name "sslImplementationName" --value "org.apache.tomcat.util.net.openssl.OpenSSLImplementation" \
              --subnode '$connector_node' --type elem --name "UpgradeProtocol"    \
              --var upgrade_node '$prev' \
              --insert '$upgrade_node' --type attr --name "className" --value "org.apache.coyote.http2.Http2Protocol" \
              --subnode '$connector_node' --type elem --name "SSLHostConfig"    \
              --var host_node '$prev' \
              --insert '$host_node' --type attr --name "protocols" --value "TLSv1.2,TLSv1.3" \
              --subnode '$host_node' --type elem --name "Certificate"    \
              --var certificate_node '$prev' \
              --insert '$certificate_node' --type attr --name "certificateKeystoreFile" --value "conf/tomcat.pfx"   \
              --insert '$certificate_node' --type attr --name "certificateKeystorePassword" --value "azerty"   \
              conf/server.xml.tmp

mv conf/server.xml.tmp conf/server.xml
