= Docker images for examples

== Database

If you need to run a PostgreSQL database for an application :

[source, subs="verbatim,quotes"]
----
docker/database.sh
----

== Tomcat HTTP/2

The image is an example for HTTP/2 in Apache Tomcat.

[source, subs="verbatim,quotes"]
----
cd docker/tomcat-http2
docker build -t jtips/tomcat-http2 .
docker run --publish 127.0.0.1:8080:8080                      \
           --publish 127.0.0.1:8442:8442                      \
           --publish 127.0.0.1:8443:8443                      \
           --publish 127.0.0.1:8444:8444                      \
           --rm --name tomcat-http2 -d jtips/tomcat-http2
----
