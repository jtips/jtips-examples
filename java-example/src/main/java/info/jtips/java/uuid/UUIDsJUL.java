package info.jtips.java.uuid;

import com.fasterxml.uuid.EthernetAddress;
import com.fasterxml.uuid.Generators;
import com.fasterxml.uuid.impl.NameBasedGenerator;
import com.fasterxml.uuid.impl.RandomBasedGenerator;
import com.fasterxml.uuid.impl.TimeBasedEpochGenerator;
import com.fasterxml.uuid.impl.TimeBasedGenerator;
import com.fasterxml.uuid.impl.TimeBasedReorderedGenerator;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import static info.jtips.java.uuid.UUIDs.printLoop;

public class UUIDsJUL {

    public static void main(String[] args) throws InterruptedException, NoSuchAlgorithmException {
        TimeBasedGenerator generatorV1 = Generators.timeBasedGenerator(EthernetAddress.fromPreferredInterface());
        printLoop(generatorV1::generate);
        TimeBasedGenerator generatorV1Random = Generators.timeBasedGenerator();
        printLoop(generatorV1Random::generate);

        NameBasedGenerator generatorV3 = Generators.nameBasedGenerator(null, MessageDigest.getInstance("MD5"));
        printLoop(() -> generatorV3.generate("JTips"));

        RandomBasedGenerator generatorV4 = Generators.randomBasedGenerator();
        printLoop(generatorV4::generate);

        NameBasedGenerator generatorV5 = Generators.nameBasedGenerator();
        printLoop(() -> generatorV5.generate("JTips"));

        TimeBasedReorderedGenerator generatorV6 = Generators.timeBasedReorderedGenerator();
        printLoop(generatorV6::generate);

        TimeBasedEpochGenerator generatorV7 = Generators.timeBasedEpochGenerator();
        printLoop(generatorV7::generate);
    }

}
