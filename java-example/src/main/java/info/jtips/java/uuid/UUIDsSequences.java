package info.jtips.java.uuid;

import com.fasterxml.uuid.EthernetAddress;
import com.fasterxml.uuid.Generators;
import com.fasterxml.uuid.impl.TimeBasedEpochGenerator;
import com.fasterxml.uuid.impl.TimeBasedGenerator;
import com.fasterxml.uuid.impl.TimeBasedReorderedGenerator;

import java.security.NoSuchAlgorithmException;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.UUID;

public class UUIDsSequences {

    public static void main(String[] args) throws InterruptedException, NoSuchAlgorithmException {
        TimeBasedGenerator gen1 = Generators.timeBasedGenerator(EthernetAddress.fromPreferredInterface());
        TimeBasedReorderedGenerator gen6 = Generators.timeBasedReorderedGenerator(EthernetAddress.fromPreferredInterface());
        TimeBasedEpochGenerator gen7 = Generators.timeBasedEpochGenerator();
        printDetail(gen1, 0L);
        printDetail(gen6, 0L);
        printDetail(gen7, 0L);
        printDetail(gen1, 123456789L);
        printDetail(gen6, 123456789L);
        printDetail(gen7, 123456789L / 10000);
        printDetail(gen1, 12345678901234L);
        printDetail(gen6, 12345678901234L);
        printDetail(gen7, 12345678901234L / 10000);
        printDetail(gen1, 123456789012345678L);
        printDetail(gen6, 123456789012345678L);
        printDetail(gen7, 123456789012345678L / 10000);
        printDetail(gen7, 123456789012345678L / 100000);
    }

    private static void printDetail(TimeBasedReorderedGenerator generator, long timestamp) {
        printDetail(generator.construct(timestamp), fromGregorianTimestamp(timestamp));
    }
    private static void printDetail(TimeBasedGenerator generator, long timestamp) {
        printDetail(generator.construct(timestamp), fromGregorianTimestamp(timestamp));
    }
    private static void printDetail(TimeBasedEpochGenerator generator, long timestamp) {
        printDetail(generator.construct(timestamp), LocalDateTime.ofInstant(Instant.ofEpochMilli(timestamp), ZoneOffset.UTC));
    }

    private static void printDetail(UUID uuid, LocalDateTime instant) {
        System.out.print(uuid);
        System.out.printf(", " + instant);
        System.out.println();
    }

    private static LocalDateTime fromGregorianTimestamp(long timestamp) {
        LocalDate origin = LocalDate.parse("1582-10-15");
        Duration delta = Duration.between(origin.atStartOfDay(), LocalDate.EPOCH.atStartOfDay());
        LocalDateTime instant = LocalDateTime.ofInstant(Instant.ofEpochMilli(timestamp / 10_000), ZoneOffset.UTC).minus(delta);
        return instant;
    }

}
