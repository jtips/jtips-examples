package info.jtips.java.uuid;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.UUID;
import java.util.function.Supplier;

public class UUIDs {

    static void printLoop(Supplier<UUID> generator) throws InterruptedException {
        for (int i = 0; i < 4; i++) {
            printDetail(generator.get());
            Thread.sleep(789);
        }
        System.out.println("------------------");
    }

    static void printDetail(UUID uuid) {
        System.out.printf("%s  // v%s", uuid, uuid.version());
        try {
            LocalDate origin = LocalDate.parse("1582-10-15");
//            Period delta = Period.between(origin.atStartOfDay(), Instant.EPOCH);
            Duration delta = Duration.between(origin.atStartOfDay(), LocalDate.EPOCH.atStartOfDay());
            System.out.printf(", timestamp " + LocalDateTime.ofInstant(Instant.ofEpochMilli(uuid.timestamp()/10_000), ZoneOffset.UTC).minus(delta));
            System.out.printf(", clockSequence " + uuid.clockSequence());
        } catch (UnsupportedOperationException ignored) {
            //System.out.printf(", " + e.getMessage());
        }
        System.out.println();
    }

}
