package info.jtips.java.uuid;

import com.github.f4b6a3.uuid.UuidCreator;

import static info.jtips.java.uuid.UUIDs.printLoop;

public class UUIDsCreator {

    public static void main(String[] args) throws InterruptedException {
        printLoop(UuidCreator::getTimeBased);
        printLoop(UuidCreator::getTimeBasedWithMac);
        printLoop(() -> UuidCreator.getNameBasedMd5("JTips"));
        printLoop(UuidCreator::getRandomBased);
        printLoop(() -> UuidCreator.getNameBasedSha1("JTips"));

        printLoop(UuidCreator::getTimeOrdered);
        printLoop(UuidCreator::getTimeOrderedEpoch);
    }

}
