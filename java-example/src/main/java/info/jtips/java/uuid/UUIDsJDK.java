package info.jtips.java.uuid;

import java.security.NoSuchAlgorithmException;
import java.util.UUID;

import static info.jtips.java.uuid.UUIDs.printLoop;

public class UUIDsJDK {

    public static void main(String[] args) throws InterruptedException, NoSuchAlgorithmException {
        printLoop(() -> UUID.nameUUIDFromBytes("JTips".getBytes()));
        printLoop(UUID::randomUUID);
    }

}
