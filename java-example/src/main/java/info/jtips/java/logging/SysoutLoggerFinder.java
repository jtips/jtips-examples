package info.jtips.java.logging;

public class SysoutLoggerFinder extends System.LoggerFinder {
  @Override
  public System.Logger getLogger(String name, Module module) {
    return new SysoutLogger(name, module);
  }
}
