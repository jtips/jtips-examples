package info.jtips.java.logging;

import java.util.ResourceBundle;

public class SysoutLogger implements System.Logger {
  private final String name;
  private final Module module;
  private final LoggerConfiguration configuration;

  public SysoutLogger(String name, Module module) {
    this.name = name;
    this.module = module;
    this.configuration = LoggerConfiguration.configuration(name);
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public boolean isLoggable(Level level) {
    return this.configuration.level().compareTo(level) <= 0;
  }

  @Override
  public void log(Level level, ResourceBundle bundle, String msg, Throwable thrown) {
    this.log(level, bundle, msg);
  }

  @Override
  public void log(Level level, ResourceBundle bundle, String msg, Object... params) {
    if (isLoggable(level)) {
      System.out.println(configuration.output(String.format(msg, params), level, name));
    }
  }
}
