package info.jtips.java.logging;

import java.lang.System.Logger.Level;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.EnumSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class LoggerConfiguration {

  private static final ConcurrentMap<String, LoggerConfiguration> configurations = new ConcurrentHashMap<>();
  private final DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss.SSS");

  private Level level = Level.INFO;
  private final Set<PatternElement> elements = EnumSet.allOf(PatternElement.class);

  public static LoggerConfiguration configuration(String name) {
    return configurations.computeIfAbsent(name, key -> new LoggerConfiguration());
  }

  private LoggerConfiguration() {
    elements.remove(PatternElement.NAME);
  }

  public LoggerConfiguration level(Level level) {
    this.level = level;
    return this;
  }

  Level level() {
    return this.level;
  }

  public LoggerConfiguration skipThread() {
    this.elements.remove(PatternElement.THREAD);
    return this;
  }

  public LoggerConfiguration skipLevel() {
    this.elements.remove(PatternElement.LEVEL);
    return this;
  }

  public LoggerConfiguration withName() {
    this.elements.add(PatternElement.NAME);
    return this;
  }

  public LoggerConfiguration skipTime() {
    this.elements.remove(PatternElement.TIME);
    return this;
  }

  String output(String msg, Level level, String name) {
    return (elements.contains(PatternElement.TIME) ? timeFormatter.format(LocalTime.now()) + " " : "")
        + (elements.contains(PatternElement.THREAD) ? '[' + Thread.currentThread().getName() + "] " : "")
        + (elements.contains(PatternElement.LEVEL) ? level + " " : "")
        + (elements.contains(PatternElement.NAME) ? name + " - " : "")
        + msg;
  }

  private enum PatternElement {
    THREAD, LEVEL, TIME, NAME
  }
}
