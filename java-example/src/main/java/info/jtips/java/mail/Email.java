package info.jtips.java.mail;

import jakarta.activation.DataSource;

import java.util.ArrayList;
import java.util.List;

public class Email {
  final String to;
  final String from;
  final String subject;
  final String content;
  final String mimeType;
  final List<DataSource> attachments;

  private Email(String to, String from, String subject, String content, String mimeType) {
    this.to = to;
    this.from = from;
    this.subject = subject;
    this.content = content;
    this.mimeType = mimeType;
    this.attachments = new ArrayList<>();
  }

  public static Builder builder() {
    return new Builder();
  }

  public void addAttachment(DataSource dataSource) {
    this.attachments.add(dataSource);
  }

  public static class Builder {
    private String to;
    private String from;
    private String subject;
    private String content;
    private String mimeType = "text/plain";

    public Builder to(String to) {
      this.to = to;
      return this;
    }

    public Builder from(String from) {
      this.from = from;
      return this;
    }

    public Builder subject(String subject) {
      this.subject = subject;
      return this;
    }

    public Builder content(String content) {
      this.content = content;
      return this;
    }

    public Builder mimeType(String mimeType) {
      this.mimeType = mimeType;
      return this;
    }

    public Email build() {
      return new Email(to, from, subject, content, mimeType);
    }
  }
}
