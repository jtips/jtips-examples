package info.jtips.java.mail;

import jakarta.activation.DataHandler;
import jakarta.activation.DataSource;
import jakarta.activation.FileDataSource;
import jakarta.mail.Authenticator;
import jakarta.mail.Message;
import jakarta.mail.MessagingException;
import jakarta.mail.Multipart;
import jakarta.mail.PasswordAuthentication;
import jakarta.mail.Session;
import jakarta.mail.Transport;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeBodyPart;
import jakarta.mail.internet.MimeMessage;
import jakarta.mail.internet.MimeMultipart;
import org.zalando.fauxpas.FauxPas;

import java.util.Properties;

import static org.zalando.fauxpas.FauxPas.throwingConsumer;
import static org.zalando.fauxpas.FauxPas.throwingFunction;

public class EmailSender {

  private final Session session;

  public EmailSender(String hostname, int port) {
    Properties properties = System.getProperties();
    properties.setProperty("mail.smtp.host", hostname);
    properties.setProperty("mail.smtp.port", String.valueOf(port));
    session = Session.getDefaultInstance(properties);
  }

  public EmailSender(String hostname, int port, String username, String password) {
    Authenticator authenticator = new Authenticator() {
      protected PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(username, password);
      }
    };
    Properties properties = System.getProperties();
    properties.setProperty("mail.smtp.host", hostname);
    properties.setProperty("mail.smtp.port", String.valueOf(port));
    properties.setProperty("mail.smtp.submitter", username);
    properties.setProperty("mail.smtp.auth", "true");
    session = Session.getDefaultInstance(properties, authenticator);

  }

  public void send(Email email) throws MessagingException {
    MimeMessage message = new MimeMessage(session);
    if (email.attachments.isEmpty()) {
      message.setFrom(new InternetAddress(email.from));
      message.addRecipient(Message.RecipientType.TO, new InternetAddress(email.to));
      message.setSubject(email.subject);
      message.setText(email.content);
    } else {
      message.setFrom(new InternetAddress(email.from));
      message.addRecipient(Message.RecipientType.TO, new InternetAddress(email.to));
      message.setSubject(email.subject);

      Multipart multipart = new MimeMultipart();
      MimeBodyPart mainBodyPart = new MimeBodyPart();
      mainBodyPart.setText(email.content);
      multipart.addBodyPart(mainBodyPart);

      email.attachments.stream().map(throwingFunction(
          source -> {
            MimeBodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setDataHandler(new DataHandler(source));
            messageBodyPart.setFileName(source.getName());
            return messageBodyPart;
          })
      ).forEach(throwingConsumer(multipart::addBodyPart)
      );
      message.setContent(multipart);
    }

    Transport.send(message);
    System.out.println("Sent message successfully....");
  }

}
