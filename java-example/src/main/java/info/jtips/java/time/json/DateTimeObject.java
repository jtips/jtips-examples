package info.jtips.java.time.json;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.Temporal;

public class DateTimeObject {

  public Instant instant;
  public LocalDate localDate;
  public LocalDateTime localDateTime;
  public LocalTime localTime;
  public OffsetDateTime offsetDateTime;
  public OffsetTime offsetTime;
  public ZonedDateTime zonedDateTime;
  public ZonedDateTime utcDateTime;

  public static DateTimeObject now() {
    DateTimeObject dateTimeObject = new DateTimeObject();
    dateTimeObject.instant = Instant.now();
    dateTimeObject.localDate = LocalDate.now();
    dateTimeObject.localDateTime = LocalDateTime.now();
    dateTimeObject.localTime = LocalTime.now();
    dateTimeObject.offsetDateTime = OffsetDateTime.now();
    dateTimeObject.offsetTime = OffsetTime.now();
    dateTimeObject.zonedDateTime = ZonedDateTime.now();
    dateTimeObject.utcDateTime = ZonedDateTime.now(ZoneOffset.UTC);
    return dateTimeObject;
  }

  public static DateTimeObject ofInstant(Instant instant, ZoneId zoneId) {
    DateTimeObject dateTimeObject = new DateTimeObject();
    dateTimeObject.instant = instant;
    dateTimeObject.localDate = LocalDate.ofInstant(instant, zoneId);
    dateTimeObject.localDateTime = LocalDateTime.ofInstant(instant, zoneId);
    dateTimeObject.localTime = LocalTime.ofInstant(instant, zoneId);
    dateTimeObject.offsetDateTime = OffsetDateTime.ofInstant(instant, zoneId);
    dateTimeObject.offsetTime = OffsetTime.ofInstant(instant, zoneId);
    dateTimeObject.zonedDateTime = ZonedDateTime.ofInstant(instant, zoneId);
    dateTimeObject.utcDateTime = ZonedDateTime.ofInstant(instant, ZoneOffset.UTC);
    return dateTimeObject;
  }

  @Override
  public String toString() {
    return String.format(
        "instant=%s, localDate=%s, localDateTime=%s, localTime=%s, offsetDateTime=%s, offsetTime=%s, zonedDateTime=%s, utcDateTime=%s",
        toString(instant), toString(localDate), toString(localDateTime), toString(localTime), toString(offsetDateTime), toString(offsetTime), toString(zonedDateTime), toString(utcDateTime)
    );
  }
  private String toString(Temporal temporal) {
    return temporal == null ? "" : String.format("%s (%s)", temporal, temporal.getClass().getSimpleName());
  }

}
