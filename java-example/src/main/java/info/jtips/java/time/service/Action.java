package info.jtips.java.time.service;

public class Action {
  public void doSleep() {
    System.out.println("Sleeping ...");
  }

  public void doPlay() {
    System.out.println("Playing ...");
  }
}
