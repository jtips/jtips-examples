package info.jtips.java.time.json;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.Temporal;

public class TemporalObject {

  public Temporal instant;
  public Temporal localDate;
  public Temporal localDateTime;
  public Temporal localTime;
  public Temporal offsetDateTime;
  public Temporal offsetTime;
  public Temporal zonedDateTime;
  public Temporal utcDateTime;

  public static TemporalObject now() {
    TemporalObject temporalObject = new TemporalObject();
    temporalObject.instant = Instant.now();
    temporalObject.localDate = LocalDate.now();
    temporalObject.localDateTime = LocalDateTime.now();
    temporalObject.localTime = LocalTime.now();
    temporalObject.offsetDateTime = OffsetDateTime.now();
    temporalObject.offsetTime = OffsetTime.now();
    temporalObject.zonedDateTime = ZonedDateTime.now();
    temporalObject.utcDateTime = ZonedDateTime.now(ZoneOffset.UTC);
    return temporalObject;
  }

  @Override
  public String toString() {
    return String.format(
        "instant=%s, localDate=%s, localDateTime=%s, localTime=%s, offsetDateTime=%s, offsetTime=%s, zonedDateTime=%s, utcDateTime=%s",
        toString(instant), toString(localDate), toString(localDateTime), toString(localTime), toString(offsetDateTime), toString(offsetTime), toString(zonedDateTime), toString(utcDateTime)
    );
  }
  private String toString(Temporal temporal) {
    return temporal == null ? "" : String.format("%s (%s)", temporal, temporal.getClass().getSimpleName());
  }
}
