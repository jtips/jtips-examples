package info.jtips.java.time;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.time.zone.ZoneOffsetTransitionRule;
import java.time.zone.ZoneRules;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.Comparator.comparing;

public class DaylightExample {

  public static void main(String[] args) {
    ZoneId zone = ZoneId.of("Europe/Paris");
    ZoneRules rules = zone.getRules();
    List<ZoneOffsetTransitionRule> transitionRules = zone.getRules().getTransitionRules();
    zone.getRules().getTransitionRules().forEach(System.out::println);

//    ZoneOffsetTransition transition = zone.getRules().nextTransition(Instant.now());
//    System.out.println(transition);
//
//    List<ZoneOffsetTransition> transitions = rules.getTransitions();
//
//    marchTransition();
//    octoberTransition();

    nextTransitions();
  }

  private static void nextTransition() {
    ZoneId.getAvailableZoneIds().stream()
        .map(ZoneId::of)
        .filter(zoneId -> zoneId.getRules().nextTransition(Instant.now()) != null)
        .min(comparing(zoneId -> zoneId.getRules().nextTransition(Instant.now()).getInstant()))
        .ifPresent(zone -> System.out.println(zone + " => " + zone.getRules().nextTransition(Instant.now())));
  }

  private static void nextTransitions() {
    Instant now = Instant.now();
    Map<Instant, List<ZoneId>> byTransition = ZoneId.getAvailableZoneIds().stream()
        .map(ZoneId::of)
        .filter(zoneId -> zoneId.getRules().nextTransition(now) != null)
        .sorted(comparing(zoneId -> zoneId.getRules().nextTransition(now).getInstant()))
        .collect(Collectors.groupingBy(zoneId -> zoneId.getRules().nextTransition(now).getInstant()));

    byTransition.forEach(
        (instant, zoneIds) -> System.out.println(instant + " - " + zoneIds)
    );
//        .forEach(zone -> System.out.println(zone + " => " + zone.getRules().nextTransition(Instant.now())));
  }

  private static void marchTransition() {
    LocalDateTime localDateTime = LocalDateTime.of(2022, Month.MARCH, 27, 1, 30);
    ZonedDateTime zonedDateTime = ZonedDateTime.of(localDateTime, ZoneId.of("Europe/Paris"));
    System.out.println(zonedDateTime + " => " + zonedDateTime.plus(1, ChronoUnit.HOURS));
  }

  private static void octoberTransition() {
    LocalDateTime localDateTime = LocalDateTime.of(2022, Month.OCTOBER, 30, 2, 30);
    ZonedDateTime zonedDateTime = ZonedDateTime.of(localDateTime, ZoneId.of("Europe/Paris"));
    System.out.println(zonedDateTime + " => " + zonedDateTime.plus(1, ChronoUnit.HOURS));
  }

}
