package info.jtips.java.time;

import info.jtips.java.time.service.Action;
import info.jtips.java.time.service.ActivityService;
import info.jtips.java.time.service.DateBuilder;

import java.time.Clock;

public class ClockExample {

  public static void main(String[] args) {
    ActivityService service = new ActivityService(new Action(), new DateBuilder(), Clock.systemUTC());
    service.chooseActivityWithBuilder();
    service.chooseActivityWithClock();
  }

}
