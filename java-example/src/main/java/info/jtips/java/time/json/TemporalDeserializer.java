package info.jtips.java.time.json;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.deser.std.StdScalarDeserializer;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.fasterxml.jackson.datatype.jsr310.deser.InstantDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.JSR310DateTimeDeserializerBase;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.OffsetTimeDeserializer;

import java.io.IOException;
import java.time.format.DateTimeParseException;
import java.time.temporal.Temporal;
import java.util.Map;
import java.util.regex.Pattern;

import static org.zalando.fauxpas.FauxPas.throwingFunction;
import static org.zalando.fauxpas.FauxPas.throwingPredicate;

public class TemporalDeserializer extends StdScalarDeserializer<Temporal> {
  public static final JsonDeserializer<? extends Temporal> INSTANCE = new TemporalDeserializer();

  private static final String LOCAL_DATE = "\\d{4}-\\d{2}-\\d{2}";
  private static final String LOCAL_TIME = "\\d{2}:\\d{2}:\\d{2}[.]?\\d{0,6}";
  private static final String UTC_TIME = LOCAL_TIME + "Z";
  private static final String OFFSET_TIME = LOCAL_TIME + "[\\+|-]\\d{2}:?\\d{2}";
  private static final String DECIMAL = "^[\\+|-]?\\d+[\\.]?\\d*$";

  private static final Map<Pattern, JSR310DateTimeDeserializerBase<? extends Temporal>> deserializers = Map.of(
      toPattern(LOCAL_DATE), LocalDateDeserializer.INSTANCE,
      toPattern(LOCAL_TIME), LocalTimeDeserializer.INSTANCE,
      toPattern(LOCAL_DATE + 'T' + LOCAL_TIME), LocalDateTimeDeserializer.INSTANCE,
      toPattern(UTC_TIME), OffsetTimeDeserializer.INSTANCE,
      toPattern(OFFSET_TIME), OffsetTimeDeserializer.INSTANCE,
      toPattern(LOCAL_DATE + 'T' + UTC_TIME), InstantDeserializer.ZONED_DATE_TIME,
      toPattern(LOCAL_DATE + 'T' + OFFSET_TIME), InstantDeserializer.OFFSET_DATE_TIME,
      toPattern(DECIMAL), InstantDeserializer.INSTANT
  );

  private static Pattern toPattern(String format) {
    return Pattern.compile('^' + format + '$');
  }

  protected TemporalDeserializer() {
    this(null);
  }

  protected TemporalDeserializer(Class<?> vc) {
    super(vc);
  }

  @Override
  public Temporal deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JacksonException {
    String text = jsonParser.getText();
    return deserializers.entrySet()
        .stream()
        .filter(throwingPredicate(entry -> entry.getKey().matcher(text).matches()))
        .findFirst()
        .map(throwingFunction(entry -> entry.getValue().deserialize(jsonParser, deserializationContext)))
        .orElseThrow(() -> new InvalidFormatException(jsonParser, "Bof", text, Temporal.class));
  }

}
