package info.jtips.java.time.service;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.MonthDay;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.Year;
import java.time.YearMonth;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class DateBuilder {

  public Instant currentInstant() {
    return Instant.now();
  }

  public LocalDate currentLocalDate() {
    return LocalDate.now();
  }
  public LocalDate currentLocalDate(ZoneId zoneId) {
    return LocalDate.now(zoneId);
  }

  public LocalDateTime currentLocalDateTime() {
    return LocalDateTime.now();
  }
  public LocalDateTime currentLocalDateTime(ZoneId zoneId) {
    return LocalDateTime.now(zoneId);
  }

  public LocalTime currentLocalTime() {
    return LocalTime.now();
  }
  public LocalTime currentLocalTime(ZoneId zoneId) {
    return LocalTime.now(zoneId);
  }

  public MonthDay currentMonthDay() {
    return MonthDay.now();
  }
  public MonthDay currentMonthDay(ZoneId zoneId) {
    return MonthDay.now(zoneId);
  }

  public OffsetDateTime currentOffsetDateTime() {
    return OffsetDateTime.now();
  }
  public OffsetDateTime currentOffsetDateTime(ZoneId zoneId) {
    return OffsetDateTime.now(zoneId);
  }

  public OffsetTime currentOffsetTime() {
    return OffsetTime.now();
  }
  public OffsetTime currentOffsetTime(ZoneId zoneId) {
    return OffsetTime.now(zoneId);
  }

  public Year currentYear() {
    return Year.now();
  }
  public Year currentYear(ZoneId zoneId) {
    return Year.now(zoneId);
  }

  public YearMonth currentYearMonth() {
    return YearMonth.now();
  }
  public YearMonth currentYearMonth(ZoneId zoneId) {
    return YearMonth.now(zoneId);
  }

  public ZonedDateTime currentZonedDateTime() {
    return ZonedDateTime.now();
  }
  public ZonedDateTime currentZonedDateTime(ZoneId zoneId) {
    return ZonedDateTime.now(zoneId);
  }}
