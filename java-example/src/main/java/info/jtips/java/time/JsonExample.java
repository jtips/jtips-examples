package info.jtips.java.time;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import info.jtips.java.time.json.DateTimeObject;
import info.jtips.java.time.json.InstantTimestampSerializer;
import info.jtips.java.time.json.TemporalDeserializer;
import info.jtips.java.time.json.TemporalObject;

import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.Temporal;

public class JsonExample {

  public static void main(String[] args) throws JsonProcessingException {
    ObjectMapper mapper = buildObjectMapper();

    Instant instant = Instant.parse("2000-01-23T01:23:45.678Z");
    doIt(mapper, DateTimeObject.ofInstant(instant, ZoneId.systemDefault()));
    doIt(mapper, TemporalObject.now());
  }

  private static void doIt(ObjectMapper mapper, Object object) throws JsonProcessingException {
    System.out.println("====== " + object.getClass());

    ObjectWriter writer = mapper.writerWithDefaultPrettyPrinter();
    System.out.println("= write");
    System.out.println(writer.writeValueAsString(object));

    String input = "{" +
        "  \"instant\":\"654.321\"," +
        "  \"localDate\":\"1970-01-01\"," +
        "  \"localDateTime\":\"1970-01-01T02:00:00\"," +
        "  \"localTime\":\"02:00:00\"," +
        "  \"offsetDateTime\":\"1970-01-01T02:00:00+02:00\"," +
        "  \"offsetTime\":\"02:00:00.000+02:00\"," +
        "  \"zonedDateTime\":\"1970-01-01T02:00:00+02:00\"," +
        "  \"utcDateTime\":\"1970-01-01T00:00:00Z\"" +
        "}";
    Object readObject = mapper.readValue(input, object.getClass());
    System.out.println("= read");
    System.out.println(readObject);
  }

  private static ObjectMapper buildObjectMapper() {
    ObjectMapper mapper = new ObjectMapper();
    JavaTimeModule javaTimeModule = new JavaTimeModule();
    javaTimeModule.addDeserializer(Temporal.class, TemporalDeserializer.INSTANCE);
    javaTimeModule.addSerializer(Instant.class, InstantTimestampSerializer.INSTANCE);
    mapper.registerModule(javaTimeModule);
    mapper.enable(SerializationFeature.WRITE_DATES_WITH_ZONE_ID);
    mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    return mapper;
  }

}
