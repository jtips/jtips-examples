package info.jtips.java.time.service;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoField;

public class ActivityService {

  private final Action action;
  private final DateBuilder dateBuilder;
  private final Clock clock;

  public ActivityService(Action action, DateBuilder dateBuilder, Clock clock) {
    this.clock = clock;
    this.action = action;
    this.dateBuilder = dateBuilder;
  }

  public void chooseActivityWithBuilder() {
    System.out.println(dateBuilder.currentLocalDateTime());
    if (dateBuilder.currentLocalDateTime().get(ChronoField.AMPM_OF_DAY) == 0) {
      action.doSleep();
    } else {
      action.doPlay();
    }
  }

  public void chooseActivityWithClock() {
    System.out.println(LocalDateTime.now(clock));
    if (LocalDateTime.now(clock).get(ChronoField.AMPM_OF_DAY) == 0) {
      action.doSleep();
    } else {
      action.doPlay();
    }
  }

  public void chooseActivityWithClockInTimezone(ZoneId zoneId) {
    if (LocalDateTime.now(clock.withZone(zoneId)).get(ChronoField.AMPM_OF_DAY) == 0) {
      action.doSleep();
    } else {
      action.doPlay();
    }
  }
}
