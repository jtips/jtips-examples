package info.jtips.java.time.json;

import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.datatype.jsr310.ser.InstantSerializer;

public class InstantTimestampSerializer extends InstantSerializer {
  public static final InstantTimestampSerializer INSTANCE = new InstantTimestampSerializer();

  public InstantTimestampSerializer() {
    super(InstantSerializer.INSTANCE, true, null);
  }

  public JsonSerializer<?> createContextual(SerializerProvider prov, BeanProperty property) {
    return this;
  }

}
