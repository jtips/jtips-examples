package info.jtips.java.executor;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CountedCompleter;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class CountedCompleterExample {

  public static void main(String[] args) {
    Random random = new SecureRandom();
    List<Integer> source = IntStream.range(1, 30_000)
        .map(random::nextInt)
        .boxed()
        .collect(Collectors.toList());

    ForkJoinPool pool = ForkJoinPool.commonPool();

    Integer max = pool.invoke(new FindMaxTask(null, source));
    System.out.println(max);
  }

  static class FindMaxTask extends CountedCompleter<Integer> {

    private final List<Integer> data;
    private final List<FindMaxTask> children = new ArrayList<>();
    private int result;

    public FindMaxTask(CountedCompleter parent, List<Integer> data) {
      super(parent);
      this.data = data;
    }

    @Override
    public void compute() {
      if (data.size() <= 3000) {
        int max = -99;
        for (int value : data) {
          max = Integer.max(max, value);
        }
        result = max;
      } else {
        int middle = data.size() / 2;
        List<Integer> leftList = data.subList(0, middle);
        List<Integer> rightList = data.subList(middle, data.size());

//        this.addToPendingCount(2);
//        new FindMaxTask(this, leftList).fork();
//        new FindMaxTask(this, rightList).fork();

        children.add(new FindMaxTask(this, leftList));
        children.add(new FindMaxTask(this, rightList));
        this.addToPendingCount(children.size());
        children.forEach(ForkJoinTask::fork);

      }
      tryComplete();
    }

    @Override
    public void onCompletion(CountedCompleter<?> caller) {
      result = Integer.max(result, children.stream().map(FindMaxTask::getRawResult)
          .mapToInt(value -> value)
          .max().orElse(0));
    }

    @Override
    public Integer getRawResult() {
      return result;
    }
  }

}
