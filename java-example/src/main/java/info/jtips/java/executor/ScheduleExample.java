package info.jtips.java.executor;

import info.jtips.java.logging.LoggerConfiguration;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ScheduleExample {

  private static final ScheduleLogger logger = new ScheduleLogger(ScheduleExample.class);

  public static void main(String[] args) {
    ScheduledThreadPoolExecutor scheduler = new ScheduledThreadPoolExecutor(5);

    scheduler
        .execute(() -> logger.logAndWait("execute", 200));
    scheduler
        .schedule(() -> logger.logAndWait("single ", 200), 10, TimeUnit.SECONDS);
    scheduler
        .scheduleAtFixedRate(() -> logger.logAndWait("rate  ", 200), 3, 10, TimeUnit.SECONDS);
    scheduler
        .scheduleWithFixedDelay(() -> logger.logAndWait("delay ", 200), 3, 10, TimeUnit.SECONDS);
    System.out.println("End of scheduling");

    scheduler.setContinueExistingPeriodicTasksAfterShutdownPolicy(true);
    scheduler.shutdown();

  }

}
