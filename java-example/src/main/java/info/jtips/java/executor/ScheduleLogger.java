package info.jtips.java.executor;

import info.jtips.java.logging.LoggerConfiguration;

import java.time.Instant;

public class ScheduleLogger {

  private final System.Logger systemLogger;

  public ScheduleLogger(Class<?> clazz) {
    LoggerConfiguration.configuration(clazz.getSimpleName())
        .withName()
        .skipLevel();
    this.systemLogger = System.getLogger(clazz.getSimpleName());
  }

  public Instant logAndWait(String type, int delay) {
    systemLogger.log(System.Logger.Level.INFO, "Scheduled " + type);
    try {
      Thread.sleep(delay);
    } catch (InterruptedException e) {
      Thread.currentThread().interrupt();
    }
    return Instant.now();
  }

}
