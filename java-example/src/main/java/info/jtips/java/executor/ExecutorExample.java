package info.jtips.java.executor;

import java.time.Instant;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static org.zalando.fauxpas.FauxPas.throwingFunction;

public class ExecutorExample {

  private static final ScheduleLogger logger = new ScheduleLogger(ExecutorExample.class);

  public static void main(String[] args) throws ExecutionException, InterruptedException {
    ExecutorService executor = Executors.newCachedThreadPool();

    executor.execute(() -> logger.logAndWait("execute 0", 1234));
    Future<Instant> future0 = executor.submit(() -> logger.logAndWait("execute", 1234));
    future0.cancel(true);

    List<Future<Instant>> futures = executor.invokeAll(List.of(
        () -> logger.logAndWait("execute 1/4", 123),
        () -> logger.logAndWait("execute 2/4", 234),
        () -> logger.logAndWait("execute 3/4", 345),
        () -> logger.logAndWait("execute 4/4", 456)
    ));

    Instant result = executor.invokeAny(List.of(
        () -> logger.logAndWait("execute 1/4", 123),
        () -> logger.logAndWait("execute 2/4", 234),
        () -> logger.logAndWait("execute 3/4", 345),
        () -> logger.logAndWait("execute 4/4", 456)
    ));

    System.out.println("Future n°0 result = " + future0.get());
    futures.stream()
        .map(throwingFunction(Future::get))
        .map(instant -> "Future result = " + instant)
        .forEach(System.out::println);
    executor.shutdown();
  }

}
