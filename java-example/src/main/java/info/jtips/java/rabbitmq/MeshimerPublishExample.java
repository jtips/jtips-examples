package info.jtips.java.rabbitmq;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

public class MeshimerPublishExample {

    public static void main(String[] args) throws IOException, TimeoutException, ExecutionException, InterruptedException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        factory.setPort(5672);
        factory.setVirtualHost("/");
        factory.setUsername("");
        factory.setPassword("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI5NDJiNDExZi1lNDE0LTQ0NjgtYjk4Yy04ZDQ5MTU2M2M3ZDgiLCJhdWQiOiJyYWJiaXRtcSIsImFnZW50SWQiOiIwZTM5YWYwYy03YjhlLTQ1NTItYmMwNS1iODQ0YWVkYzNiNGQiLCJzY29wZSI6WyJyYWJiaXRtcS5yZWFkOiUyRi9xLmFnZW50LjBlMzlhZjBjLTdiOGUtNDU1Mi1iYzA1LWI4NDRhZWRjM2I0ZCIsInJhYmJpdG1xLndyaXRlOiUyRi94LmNsaWVudC45NDJiNDExZi1lNDE0LTQ0NjgtYjk4Yy04ZDQ5MTU2M2M3ZDgudXBsb2FkLyoiXSwiZXhwIjoxNzAyMjQ1NTM5LCJpYXQiOjE2OTk2NTM1Mzl9.hQiqBsP0ERCzlAgGTU7MmRAfqYAgw-PtoMLYUmJFAZY");

        try (
            Connection connection = factory.newConnection();
            Channel channel = connection.createChannel()
        ) {
            channel.basicPublish(
                    "x.client.942b411f-e414-4468-b98c-8d491563c7d8.upload",
                    "agent.0e39af0c-7b8e-4552-bc05-b844aedc3b4d",
                    new AMQP.BasicProperties().builder().headers(Map.of("eventType", "heartbeat")).build(),
                    "Ping".getBytes()
            );
            System.out.println("Published");
        }
    }

}
