package info.jtips.java.rabbitmq;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

public class MeshimerConsumeExample {

    public static void main(String[] args) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("rabbitmqbackend.dev.meshimer.io");
        factory.setPort(5672);
        factory.setVirtualHost("/dev");
        factory.setUsername("");
        factory.setPassword("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI5NDJiNDExZi1lNDE0LTQ0NjgtYjk4Yy04ZDQ5MTU2M2M3ZDgiLCJhdWQiOiJyYWJiaXRtcSIsImFnZW50SWQiOiIyN2FmZWVkNS1mYmFmLTQwZDktYjZjMS0yZWVmOTg3YTZkNzIiLCJzY29wZSI6WyJyYWJiaXRtcS53cml0ZTolMkZkZXYvcS5hZ2VudC4yN2FmZWVkNS1mYmFmLTQwZDktYjZjMS0yZWVmOTg3YTZkNzIiLCJyYWJiaXRtcS5jb25maWd1cmU6JTJGZGV2L3EuYWdlbnQuMjdhZmVlZDUtZmJhZi00MGQ5LWI2YzEtMmVlZjk4N2E2ZDcyIiwicmFiYml0bXEucmVhZDolMkZkZXYvcS5hZ2VudC4yN2FmZWVkNS1mYmFmLTQwZDktYjZjMS0yZWVmOTg3YTZkNzIiLCJyYWJiaXRtcS5yZWFkOiUyRmRldi94LmNsaWVudC45NDJiNDExZi1lNDE0LTQ0NjgtYjk4Yy04ZDQ5MTU2M2M3ZDguYWdlbnQvYWdlbnQuMjdhZmVlZDUtZmJhZi00MGQ5LWI2YzEtMmVlZjk4N2E2ZDcyIiwicmFiYml0bXEucmVhZDolMkZkZXYvcS5jbGllbnQuOTQyYjQxMWYtZTQxNC00NDY4LWI5OGMtOGQ0OTE1NjNjN2Q4Lmdsb2JhbCJdLCJleHAiOjE3MDEzNzMyODAsImlhdCI6MTY5ODc4MTI4MH0.b9qobOF-yIcfa0Ug0DSXiNgySZIX2hn3KLwo6g8zu78");

        try (
                Connection connection = factory.newConnection();
                Channel channel = connection.createChannel()
        ) {
            CompletableFuture<byte[]> response = new CompletableFuture<>();
            channel.basicQos(1);
            channel.basicConsume(
                    "q.client.942b411f-e414-4468-b98c-8d491563c7d8.global",
                    new DefaultConsumer(channel) {
                        @Override
                        public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) {
                            System.out.println("Consumed " + new String(body));
                            response.complete(body);
                        }
                    });
            response.get();
        }
    }
}
