package info.jtips.java.rabbitmq;

import java.util.UUID;

public record Activity(UUID id, String type) {
}
