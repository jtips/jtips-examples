package info.jtips.java.rabbitmq;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.*;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Base64;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

/**
 * Needs a RabbitMQ broker
 */
public class RabbitMqExample {

    private static final ObjectMapper json = new ObjectMapper();
    private static Connection connection;
    //amqp://gazzz:
    // rabbitmqbackend.dev.meshimer.io:5672//dev
    private static final Access access = Access.builder()
            .hostname("rabbitmqbackend.dev.meshimer.io")
            .port(5672)
            .virtualHost("/dev")
            .username("zzz")
            .password("")
            .build();

    public static void main(String[] args) throws IOException, TimeoutException, ExecutionException, InterruptedException {
        try (Connection con = connect()) {
            connection = con;

            createDurableQueue();
            createDirectExchange();
            bindQueueToExchange();

            publishMessage();
            getMessage();

            publishMessage();
            consumeMessage();

            exportDefinitions();
        }
    }

    private static Connection connect() throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(access.hostname);
        factory.setPort(access.port);
        factory.setVirtualHost(access.virtualHost);
        // "guest"/"guest", limited to localhost connections
        factory.setUsername(access.username);
        factory.setPassword(access.password);

        return factory.newConnection();
    }

    private static void createDurableQueue() throws IOException {
        Channel channel = connection.createChannel();
        channel.queueDeclare(
                "q.activity.ride",
                true,         // durable
                false,        // not exclusive,
                false,        // not auto-delete
                Map.of()      // no argument
        );
    }

    private static void createDirectExchange() throws IOException {
        Channel channel = connection.createChannel();
        channel.exchangeDeclare(
                "x.activity",
                "direct"      // type
        );
    }

    private static void bindQueueToExchange() throws IOException {
        Channel channel = connection.createChannel();
        channel.queueBind(
                "q.activity.ride",
                "x.activity",
                "ride"         // routing key
        );
    }

    private static void publishMessage() throws IOException {
        Activity activity = new Activity(UUID.randomUUID(), "ride");
        Channel channel = connection.createChannel();
        channel.basicPublish(
                "x.activity",
                activity.type(),
                null,
                json.writeValueAsString(activity).getBytes()
        );
        System.out.println("Published " + activity);
    }

    private static void getMessage() throws IOException {
        Channel channel = connection.createChannel();
        GetResponse response = channel.basicGet("q.activity.ride", true);
        Activity readActivity = json.readerFor(Activity.class).readValue(response.getBody());
        System.out.println("Got " + readActivity);
    }

    private static void consumeMessage() throws IOException, ExecutionException, InterruptedException {
        CompletableFuture<Activity> response = new CompletableFuture<>();
        Channel channel = connection.createChannel();
        channel.basicConsume(
                "q.activity.ride",
                new DefaultConsumer(channel) {
                    @Override
                    public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                        response.complete(json.readerFor(Activity.class).readValue(body));
                    }
                });
        System.out.println("Consumed " + response.get());
    }

    private static void exportDefinitions() throws IOException, InterruptedException {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://%s:%s/api/definitions".formatted(access.hostname(), access.managementPort())))
                .header("Authorization", "Basic " + Base64.getEncoder().encodeToString((access.username() + ":" + access.password()).getBytes()))
                .build();

        HttpResponse<String> response = client.send(
                request, HttpResponse.BodyHandlers.ofString());
//        System.out.println(response.headers());
        System.out.println(response.body());
    }

    private record Access(String hostname, Integer port, Integer managementPort, String virtualHost, String username,
                          String password) {
        public static Builder builder() {
            return new Builder();
        }

        public static class Builder {
            private String hostname = "localhost";
            private Integer port = 5672;
            private Integer managementPort = 15672;
            private String virtualHost = "/";
            private String username = "guest";
            private String password = "guest";

            public Builder hostname(String hostname) {
                this.hostname = hostname;
                return this;
            }

            public Builder port(Integer port) {
                this.port = port;
                return this;
            }

            public Builder managementPort(Integer managementPort) {
                this.managementPort = managementPort;
                return this;
            }

            public Builder virtualHost(String virtualHost) {
                this.virtualHost = virtualHost;
                return this;
            }

            public Builder username(String username) {
                this.username = username;
                return this;
            }

            public Builder password(String password) {
                this.password = password;
                return this;
            }

            public Access build() {
                return new Access(hostname, port, managementPort, virtualHost, username, password);
            }
        }
    }
}
