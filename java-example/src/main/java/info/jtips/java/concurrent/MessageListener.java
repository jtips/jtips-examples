package info.jtips.java.concurrent;

public interface MessageListener<D> {
  void onMessage(Message<D> message);
}
