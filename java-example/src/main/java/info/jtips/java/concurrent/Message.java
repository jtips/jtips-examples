package info.jtips.java.concurrent;

public class Message<D> {
  private D data;

  public Message(D data) {
    this.data = data;
  }

  public D getData() {
    return data;
  }

  @Override
  public String toString() {
    return "Message: " + data;
  }
}
