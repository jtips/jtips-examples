package info.jtips.java.concurrent;

public class MessageMain {

  public static void main(String[] args) throws InterruptedException {
    MessageBus messageBus = new MessageBus();

    messageBus.register("Q2", message -> System.out.printf("[%s] Listener: %s%n", Thread.currentThread().getName(), message));

    messageBus.send("Q1", new Message<>("MSG-11"));
    messageBus.send("Q1", new Message<>("MSG-12"));

    for (int i = 0; i < 1000; i++) {
      messageBus.send("Q2", new Message<>(String.format("MSG-2%04d", i)));
      Thread.sleep(1);
    }

    while (true) {
      System.out.println("Receiver: " + messageBus.receive("Q1"));
    }
  }

}
