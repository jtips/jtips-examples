package info.jtips.java.concurrent;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

public class MessageBus {
  private final ConcurrentMap<String, BlockingQueue<Message<?>>> queues = new ConcurrentHashMap<>();
  private final ExecutorService listenThreadPool = Executors.newCachedThreadPool();
  private final ExecutorService executeThreadPool = Executors.newFixedThreadPool(10);

  public void send(String name, Message<?> message) {
    BlockingQueue<Message<?>> queue = queues.computeIfAbsent(name, key -> new LinkedBlockingQueue<>());
    queue.add(message);
  }

  public <D> Message<D> receive(String name) {
    try {
      BlockingQueue<Message<?>> queue = queues.get(name);
      return queue != null ? (Message<D>) queue.take() : null;
    } catch (InterruptedException e) {
      Thread.currentThread().interrupt();
      return null;
    }
  }

  public <D> void register(String name, MessageListener<D> messageListener) {
    listenThreadPool.submit(() -> {
      while (true) {
        Message<D> message = this.receive(name);
        if (message != null) {
          executeThreadPool.submit(() -> messageListener.onMessage(message));
        }
      }
    });
  }
}
