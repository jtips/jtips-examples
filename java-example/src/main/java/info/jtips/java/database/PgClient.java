package info.jtips.java.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PgClient {

  private final String url;

  private Connection connection;

  public PgClient(String url) {
    this.url = url;
  }

  public PgClient(Object hostname, Object port, Object database) {
    url = String.format("jdbc:postgresql://%s:%s/%s", hostname, port, database);
  }

  public PgClient connect(String username, String password) throws SQLException {
    connection = DriverManager.getConnection(url, username, password);
    return this;
  }

  public void init() throws SQLException {
    connection.prepareStatement("create table JTips (code varchar primary key, title varchar)")
        .execute();
    connection.prepareStatement(
            "insert into JTips (code, title) values ('JUnit/TestContainers', 'Conteneurs Docker pour les tests d''intégration')"
        )
        .execute();
    connection.prepareStatement(
            "insert into JTips (code, title) values ('Spring/TestContainers', 'Conteneurs Docker pour les tests Spring')"
        )
        .execute();
  }

  public int count() throws SQLException {
    ResultSet resultSet = connection.prepareStatement(
            "select count(*) from JTips"
        )
        .executeQuery();
    resultSet.next();
    return resultSet.getInt(1);
  }
}
