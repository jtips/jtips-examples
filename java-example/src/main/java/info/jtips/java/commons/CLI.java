package info.jtips.java.commons;

import info.jtips.java.logging.LoggerConfiguration;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import static java.lang.System.Logger.Level.INFO;

public class CLI {

  private static final System.Logger logger = System.getLogger("CLI");

  public static void main(String[] args) {
    LoggerConfiguration.configuration("CLI")
        .skipThread()
        .skipLevel()
        .skipTime();
    Options options = buildOptions();

    try {
      CommandLineParser parser = new DefaultParser();
      CommandLine cmd = parser.parse(options, args);

      // Option obligatoire, je ne teste donc pas sa présence
      String outFile = cmd.getOptionValue('o');
      logger.log(INFO, "%s : %s", "'o'", outFile);

      // Option facultative, je teste sa présence
      boolean aValue = cmd.hasOption('a');
      logger.log(INFO, "%s : %s", "'a'", aValue);

      // Option facultative, je passe une valeur par défaut
      String bValue = cmd.getOptionValue('b', "Valeur par défaut pour b");
      logger.log(INFO, "%s : %s", "'b'", bValue);
    } catch (ParseException e) {
      System.out.println("Options incorrectes : " + e.getMessage());
      HelpFormatter formatter = new HelpFormatter();
      formatter.printHelp("CLI", options);
    }
  }

  private static Options buildOptions() {
    Options options = new Options();

    // Ajoute une option -a, sans argument, avec une description
    options.addOption("a", false, "Option a");

    // Ajoute une option -b, ou --blabla, avec un argument, avec une description
    options.addOption("b", "blabla", true, "Option blabla");

    // Création de l'option -o, ou --out-file, avec un argument
    Option outFileOption = Option.builder()
        .option("o")
        .longOpt("out-file")
        .desc("URL du fichier cible")
        .hasArg()
        .required()
        .build();
    // L'option est obligatoire
    outFileOption.setRequired(true);
    // L'option est ajoutée à la liste
    options.addOption(outFileOption);

    return options;
  }
}
