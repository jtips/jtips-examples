package info.jtips.container.mail;

public class EmailException extends RuntimeException {
  public EmailException(Exception e) {
    super(e);
  }
}
