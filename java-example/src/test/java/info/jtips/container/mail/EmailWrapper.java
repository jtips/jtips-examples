package info.jtips.container.mail;

import jakarta.activation.DataHandler;
import jakarta.mail.Address;
import jakarta.mail.BodyPart;
import jakarta.mail.Message;
import jakarta.mail.MessagingException;
import jakarta.mail.Multipart;
import jakarta.mail.Session;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeMessage;

import jakarta.activation.DataSource;
import jakarta.mail.internet.MimePart;
import jakarta.mail.util.ByteArrayDataSource;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.FileTime;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.zalando.fauxpas.FauxPas.throwingFunction;

public class EmailWrapper {

  private MimeMessage message;

  public static List<EmailWrapper> fromDirectory(Path dirPath) {
    try {
      return Files.list(dirPath)
          .map(EmailWrapper::new)
          .collect(Collectors.toList());
    } catch (IOException e) {
      throw new EmailException(e);
    }
  }

  public static Optional<EmailWrapper> latestFromDirectory(Path dirPath) {
    try {
      return Files.list(dirPath)
          .sorted(Comparator.comparing((Path file) -> {
            try {
              return (FileTime) Files.getAttribute(file, "creationTime");
            } catch (IOException e) {
              throw new EmailException(e);
            }
          }).reversed())
          .map(EmailWrapper::new)
          .findFirst();
    } catch (IOException e) {
      throw new EmailException(e);
    }
  }

  public EmailWrapper(Path source) {
    try (InputStream is = Files.newInputStream(source)) {
      Session session = Session.getDefaultInstance(new Properties());
      message = new MimeMessage(session, is);
    } catch (IOException | MessagingException e) {
      message = null;
    }
  }

  public String getFrom() {
    try {
      return toStringStream(message.getFrom())
          .findFirst()
          .orElse(null);
    } catch (MessagingException e) {
      throw new EmailException(e);
    }
  }

  public String getSubject() {
    try {
      return message.getSubject();
    } catch (MessagingException e) {
      throw new EmailException(e);
    }
  }

  public List<String> getTo() {
    try {
      return toStringStream(message.getRecipients(Message.RecipientType.TO))
          .collect(Collectors.toList());
    } catch (MessagingException e) {
      throw new EmailException(e);
    }
  }

  public List<String> getCc() {
    try {
      return toStringStream(message.getRecipients(Message.RecipientType.CC))
          .collect(Collectors.toList());
    } catch (MessagingException e) {
      throw new EmailException(e);
    }
  }

  public List<String> getBcc() {
    try {
      return toStringStream(message.getRecipients(Message.RecipientType.BCC))
          .collect(Collectors.toList());
    } catch (MessagingException e) {
      throw new EmailException(e);
    }
  }

  public String getDescription() {
    try {
      return message.getDescription();
    } catch (MessagingException e) {
      throw new EmailException(e);
    }
  }

  public Date getSentDate() {
    try {
      return message.getSentDate();
    } catch (MessagingException e) {
      throw new EmailException(e);
    }
  }

  public Date getReceivedDate() {
    try {
      return message.getReceivedDate();
    } catch (MessagingException e) {
      throw new EmailException(e);
    }
  }

  public String getReplyTo() {
    try {
      return toStringStream(message.getReplyTo())
          .findFirst()
          .orElse(null);
    } catch (MessagingException e) {
      throw new EmailException(e);
    }
  }

  public String getContent() {
    try {
      Object messageContent = message.getContent();
      if (messageContent instanceof String textContent) {
        return textContent;
      } else if (messageContent instanceof Multipart multipart) {
        return multipart.getBodyPart(0).getContent().toString();
      } else {
        return messageContent.toString();
      }
    } catch (IOException | MessagingException e) {
      throw new EmailException(e);
    }
  }

  public List<DataSource> getAttachments() {
    try {
      Object messageContent = message.getContent();
      if (messageContent instanceof Multipart multipart) {
        return IntStream.range(1, multipart.getCount())
            .boxed()
            .map(throwingFunction(multipart::getBodyPart))
            .map(part -> (MimePart) part)
            .map(throwingFunction(MimePart::getDataHandler))
            .map(throwingFunction(DataHandler::getDataSource))
            .collect(Collectors.toList());
      } else {
        return List.of();
      }
    } catch (IOException | MessagingException e) {
      throw new EmailException(e);
    }
  }

  private Stream<String> toStringStream(Address[] addresses) {
    return Arrays.stream(addresses)
        .map(address -> (InternetAddress) address)
        .map(InternetAddress::getAddress);
  }

  protected DataSource createDataSource(final Multipart parent, final MimePart part)
      throws MessagingException, IOException {
    DataHandler dataHandler = part.getDataHandler();
    DataSource dataSource = dataHandler.getDataSource();
    ByteArrayDataSource result = new ByteArrayDataSource(dataSource.getInputStream().readAllBytes(), dataSource.getContentType());
    result.setName(dataSource.getName());
    return result;
  }
}
