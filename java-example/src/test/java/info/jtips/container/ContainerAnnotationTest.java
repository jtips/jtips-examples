package info.jtips.container;

import info.jtips.java.database.PgClient;
import info.jtips.java.mail.Email;
import info.jtips.java.mail.EmailSender;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import jakarta.mail.MessagingException;

import java.sql.SQLException;

import static org.assertj.core.api.Assertions.assertThat;

@Testcontainers
public class ContainerAnnotationTest {

  @Container
  static GenericContainer<?> smtp = new GenericContainer<>("mailhog/mailhog")
      .withExposedPorts(1025, 8025);

  @Container
  PostgreSQLContainer<?> pg = new PostgreSQLContainer<>("postgres:14")
      .withDatabaseName("jtips")
      .withUsername("jtips")
      .withPassword("jtipspwd");
  private EmailSender emailSender;
  private PgClient pgClient;

  @Test
  void containers_should_be_running() {
    assertThat(smtp.isRunning()).isTrue();
    assertThat(pg.isRunning()).isTrue();
  }

  @BeforeEach
  void init() throws SQLException {
    emailSender = new EmailSender("localhost", smtp.getFirstMappedPort());
    pgClient = new PgClient("localhost", pg.getMappedPort(5432), pg.getDatabaseName())
        .connect(pg.getUsername(), pg.getPassword());
  }

  @Test
  void should_be_able_to_send_mail() throws MessagingException {
    Email email = Email.builder()
        .from("author@jtips.info")
        .to("reader@jtips.info")
        .subject("Should work")
        .content("This email should be sent.")
        .build();
    emailSender.send(email);
  }

  @Test
  void should_be_able_to_connect_to_pg() throws SQLException {
    pgClient.init();
    assertThat(pgClient.count())
        .isEqualTo(2);
  }
}
