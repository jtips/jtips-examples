package info.jtips.container;

import info.jtips.container.mail.EmailWrapper;
import info.jtips.java.database.PgClient;
import info.jtips.java.mail.Email;
import info.jtips.java.mail.EmailSender;
import jakarta.mail.util.ByteArrayDataSource;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.PostgreSQLContainer;

import jakarta.mail.MessagingException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.sql.SQLException;

import static org.assertj.core.api.Assertions.assertThat;

public class ContainerSimpleTest {

  private static final String SMTP_PATH = System.getProperty("java.io.tmpdir") + "/smtp";
  static GenericContainer<?> smtp = new GenericContainer<>("ghusta/fakesmtp")
      .withExposedPorts(25)
      .withFileSystemBind(SMTP_PATH, "/var/mail");

  PostgreSQLContainer<?> pg = new PostgreSQLContainer<>("postgres:14");

  PgClient pgClient;

  @BeforeAll
  static void initClass() {
    smtp.start();
  }

  @AfterAll
  static void cleanClass() {
    smtp.stop();
  }

  @BeforeEach
  void init() throws SQLException {
    pg.start();
    pgClient = new PgClient(pg.getJdbcUrl())
        .connect(pg.getUsername(), pg.getPassword());
  }

  @AfterEach
  void clean() throws IOException, InterruptedException {
    pg.stop();
    // Cleaning files that are persisted in volume
    smtp.execInContainer("bash", "-c", "rm /var/mail/*");
  }

  @Test
  void containers_should_be_running() {
    assertThat(smtp.isRunning()).isTrue();
    assertThat(pg.isRunning()).isTrue();
  }

  @Test
  void should_be_able_to_send_simple_mail() throws MessagingException {
    Email email = Email.builder()
        .from("author@jtips.info")
        .to("reader@jtips.info")
        .subject("Should work")
        .content("This email should be sent.")
        .build();

    EmailSender emailSender = new EmailSender("localhost", smtp.getMappedPort(25));
    emailSender.send(email);

    assertThat(countEmails()).isEqualTo(1);

    EmailWrapper receivedEmail = EmailWrapper.latestFromDirectory(Path.of(SMTP_PATH)).orElseThrow();
    assertThat(receivedEmail.getFrom())
        .isEqualTo("author@jtips.info");
    assertThat(receivedEmail.getTo())
        .containsExactly("reader@jtips.info");
    assertThat(receivedEmail.getSubject())
        .isEqualTo("Should work");
    assertThat(receivedEmail.getContent())
        .startsWith("This email should be sent");
    assertThat(receivedEmail.getAttachments())
        .isEmpty();
  }

  @Test
  void should_be_able_to_send_mail_with_attachement() throws MessagingException {
    Email email = Email.builder()
        .from("author@jtips.info")
        .to("reader@jtips.info")
        .subject("Should work")
        .content("This email should be sent with attachment.")
        .build();
    ByteArrayDataSource attachment = new ByteArrayDataSource(new byte[100], "application/octet-stream");
    attachment.setName("hello.bin");
    email.addAttachment(attachment);

    EmailSender emailSender = new EmailSender("localhost", smtp.getMappedPort(25));
    emailSender.send(email);

    assertThat(countEmails()).isEqualTo(1);

    EmailWrapper receivedEmail = EmailWrapper.latestFromDirectory(Path.of(SMTP_PATH)).orElseThrow();
    assertThat(receivedEmail.getFrom())
        .isEqualTo("author@jtips.info");
    assertThat(receivedEmail.getTo())
        .containsExactly("reader@jtips.info");
    assertThat(receivedEmail.getSubject())
        .isEqualTo("Should work");
    assertThat(receivedEmail.getContent())
        .startsWith("This email should be sent");
    assertThat(receivedEmail.getAttachments())
        .hasSize(1);
  }

  private int countEmails() {
    return new File(SMTP_PATH).listFiles().length;
  }

  @Test
  void should_be_able_to_connect_to_pg() throws SQLException {
    pgClient.init();
    assertThat(pgClient.count())
        .isEqualTo(2);
  }

  // Read eml, see
}
