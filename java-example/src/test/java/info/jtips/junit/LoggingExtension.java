package info.jtips.junit;

import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.AfterTestExecutionCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.BeforeTestExecutionCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.TestExecutionExceptionHandler;
import org.junit.jupiter.api.extension.TestInstancePostProcessor;
import org.junit.jupiter.api.extension.TestInstancePreDestroyCallback;

public class LoggingExtension implements
    BeforeAllCallback, AfterAllCallback,
    BeforeEachCallback, AfterEachCallback,
    BeforeTestExecutionCallback, AfterTestExecutionCallback,
    TestExecutionExceptionHandler,
    TestInstancePostProcessor, TestInstancePreDestroyCallback {
  private String name = LoggingExtension.class.getSimpleName();

  public static LoggingExtension named(String name) {
    LoggingExtension loggingExtension = new LoggingExtension();
    loggingExtension.name = name;
    return loggingExtension;
  }

  @Override
  public void beforeAll(ExtensionContext context) throws Exception {
    log("beforeAll");
  }

  @Override
  public void afterAll(ExtensionContext context) throws Exception {
    log("afterAll");
  }

  @Override
  public void beforeEach(ExtensionContext context) throws Exception {
    log("beforeEach");
  }

  @Override
  public void afterEach(ExtensionContext context) throws Exception {
    log("afterEach");
  }

  @Override
  public void beforeTestExecution(ExtensionContext context) throws Exception {
    log("beforeTestExecution");
  }

  @Override
  public void afterTestExecution(ExtensionContext context) throws Exception {
    log("afterTestExecution");
  }

  @Override
  public void handleTestExecutionException(ExtensionContext context, Throwable throwable) throws Throwable {
    log("handleTestExecutionException");
  }

  @Override
  public void postProcessTestInstance(Object testInstance, ExtensionContext context) throws Exception {
    log("postProcessTestInstance");
  }

  @Override
  public void preDestroyTestInstance(ExtensionContext context) throws Exception {
    log("preDestroyTestInstance");
  }

  private void log(String text) {
    System.out.println(name + " - " + text);
  }
}
