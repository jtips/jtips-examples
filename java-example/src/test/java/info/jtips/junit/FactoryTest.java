package info.jtips.junit;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.junit.jupiter.api.extension.TestInstanceFactory;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

public class FactoryTest {

  @RegisterExtension
  static TestInstanceFactory factory = (factoryContext, extensionContext) -> new FactoryTest(Instant.now().minus(1, ChronoUnit.DAYS));

  private final Instant instant;

  public FactoryTest(Instant now) {
    this.instant = now;
  }

  @Test
  void should_work() {
    System.out.println("@Test " + instant);
  }

}
