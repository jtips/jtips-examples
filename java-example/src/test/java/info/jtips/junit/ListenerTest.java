package info.jtips.junit;

import org.junit.jupiter.api.ClassOrderer;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestClassOrder;
import org.junit.jupiter.api.condition.EnabledForJreRange;
import org.junit.jupiter.api.condition.JRE;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

@TestClassOrder(ClassOrderer.OrderAnnotation.class)
public class ListenerTest {

    @Test
    public void test() {
        System.out.println("=> test()");
    }

    @ParameterizedTest
    @ValueSource(strings = {"Aaaa", "Bbbbb"})
    public void parameterizedTest(String value) {
        System.out.println("=> parameterizedTest() - " + value);
    }

    @Test
    @Disabled("TODO")
    public void disabled() {
        System.out.println("=> disabled()");
    }

    @Test @Order(1)
    @EnabledForJreRange(min = JRE.JAVA_8, max = JRE.JAVA_9)
    public void onlyOldJre() {
        System.out.println("=> onlyOldJre()");
    }

    @Nested
    @DisplayName("This is nested test class")
    class ListenerSubTest {
        @Test
        public void subTest() {
            System.out.println("=> subTest()");
        }

        @Nested
        @DisplayName("This is nested nested test class")
        class ListenerSubSubTest {
            @Test
            public void subSubTest() {
                System.out.println("=> subSubTest()");
            }
        }
    }
}
