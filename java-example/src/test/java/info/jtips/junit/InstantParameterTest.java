package info.jtips.junit;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.TestReporter;
import org.junit.jupiter.api.extension.ExtendWith;

import java.time.Instant;

@ExtendWith(InstantParameterResolver.class)
public class InstantParameterTest {

  @BeforeAll
  static void beforeAll(Instant instant) {
    System.out.println("@BeforeAll " + instant);
  }

  @BeforeEach
  void setUp(Instant instant) {
    System.out.println("@BeforeEach " + instant);
  }

  @Test
  void should_work(Instant instant) {
    System.out.println("@Test " + instant);
  }

  @Test
  void should_work(TestInfo info, TestReporter reporter) {
    System.out.println("@Test " + info.getDisplayName());
    reporter.publishEntry("Reporter OK");
  }
}
