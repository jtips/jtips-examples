package info.jtips.junit.auth;

public record User(String login, String password, String profile) {

}
