package info.jtips.junit.auth;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
public @interface UserProfile {
  String DEFAULT_PROFILE = "user";

  String value() default DEFAULT_PROFILE;
}
