package info.jtips.junit;

import org.junit.jupiter.api.Disabled;
import org.junit.platform.engine.TestSource;
import org.junit.platform.engine.support.descriptor.ClassSource;
import org.junit.platform.engine.support.descriptor.MethodSource;
import org.junit.platform.launcher.TestExecutionListener;
import org.junit.platform.launcher.TestIdentifier;
import org.junit.platform.launcher.TestPlan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.stream.Collectors;

public class DisplayNamesLoggingListener implements TestExecutionListener {

    private TestPlan testPlan;

    @Override
    public void testPlanExecutionStarted(TestPlan testPlan) {
        this.testPlan = testPlan;
    }

    @Override
    public void executionStarted(TestIdentifier testIdentifier) {
        TestSource testSource = testIdentifier.getSource().orElse(null);
        if (testSource instanceof ClassSource classSource) {
            if (testPlan.getParent(testIdentifier)
                .flatMap(TestIdentifier::getSource)
                .filter(source -> source instanceof ClassSource)
                .isPresent()) {
                // It's a nested test
                return;
            }

            System.out.printf("------------------------------------------------------------------------------------------------------------------%n" +
                " Running: %s class%n" +
                " Executing tests:%n", classSource.getJavaClass().getSimpleName());
            System.out.println(
                this.testPlan.getChildren(testIdentifier)
                    .stream()
                    .map((TestIdentifier testIdentifier1) -> buildFullDisplayName(testIdentifier1, "  - "))
                    .collect(Collectors.joining("\n"))
            );
            System.out.println("------------------------------------------------------------------------------------------------------------------");
        } else if (testSource instanceof MethodSource methodSource) {
            Logger logger = LoggerFactory.getLogger(methodSource.getJavaClass());
            logger.info("\n\n****************************************************************************************************************\n" +
                "Starting test: " + testIdentifier.getDisplayName()
                + "\n****************************************************************************************************************\n");
        }
    }

    private String buildFullDisplayName(TestIdentifier testIdentifier, String prefix) {
        return testIdentifier.getSource()
            .map(testSource -> {
                if (testSource instanceof MethodSource methodSource) {
                    String display = prefix + methodSource.getMethodName() + ": " + testIdentifier.getDisplayName();
                    Disabled disabled = methodSource.getJavaMethod().getDeclaredAnnotation(Disabled.class);
                    if (disabled != null) {
                        String disabledValue = disabled.value();
                        display += " [Disabled" + (disabledValue.isEmpty() ? "" : ", " + disabledValue) + "]";
                    }
                    return display;
                } else if (testSource instanceof ClassSource classSource) {
                    // Nested class
                    return prefix + classSource.getJavaClass().getSimpleName() + ": " + testIdentifier.getDisplayName() + "\n"
                        + this.testPlan.getChildren(testIdentifier)
                            .stream()
                            .map(testId -> buildFullDisplayName(testId, "  " + prefix))
                            .collect(Collectors.joining("\n"));
                } else {
                    return "";
                }
            })
            .orElse("");
    }
}
