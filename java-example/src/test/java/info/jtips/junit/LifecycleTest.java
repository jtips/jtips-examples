package info.jtips.junit;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestReporter;
import org.junit.jupiter.api.extension.RegisterExtension;

//@ExtendWith(LoggingExtension.class)
@Logging
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class LifecycleTest {

  @RegisterExtension
  LoggingExtension loggingExtension = LoggingExtension.named("@RegisterExtension");

  @BeforeAll
  static void beforeAll(TestReporter testReporter) {
    System.out.println("@BeforeAll");
  }

  @BeforeEach
  void setUp() {
    System.out.println("@BeforeEach");
  }

  @AfterEach
  void tearDown() {
    System.out.println("@AfterEach");
  }

  @AfterAll
  static void afterAll() {
    System.out.println("@AfterAll");
  }

  @Test
  void should_work() {
    System.out.println("@Test should_work");
  }

  @Test
  void should_throw_exception() throws Exception {
    System.out.println("@Test should_throw_exception");
    throw new Exception();
  }

  @Test @Disabled
  void should_be_ignored() {
    System.out.println("@Test should_work");
  }
}
