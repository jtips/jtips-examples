package info.jtips.junit;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.DisabledIf;
import org.junit.jupiter.api.condition.EnabledForJreRange;

import org.junit.jupiter.api.condition.JRE;

import java.time.LocalTime;

public class ConditionalTest {

  @Test
  @EnabledForJreRange(min = JRE.JAVA_11, max = JRE.JAVA_17)
  public void testForJreRange() {
    System.out.println("Enabled on JRE version: " + System.getProperty("java.version"));
  }

  @Test
  @DisabledIf("tooLate")
  public void testIf() {
    System.out.println("Enabled at hour: " + LocalTime.now());
  }

  private boolean tooLate() {
    return LocalTime.now().isAfter(LocalTime.of(22, 0));
  }

}
