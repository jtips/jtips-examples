package info.jtips.junit;

import info.jtips.junit.auth.User;
import info.jtips.junit.auth.UserProfile;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

public class AuthenticatedTest {

  @Test
  void should_work_with_admin(@UserProfile("admin") User user) {
    System.out.println(user);
//    String url = "/api/v1/protected/product";
//    ValidatableResponse response =
//        given()
//            .baseUri("http://localhost:8080")
//            .contentType(ContentType.JSON)
//            .accept(ContentType.JSON)
//            .auth().basic(user.login(), user.password())
//            .when()
//            .get(url)
//            .then()
//            .statusCode(200);
  }

  @Test
  void should_work_with_private_user(User user) {
    System.out.println(user);
  }

  @Test
  void should_work_with_public_user(@UserProfile("public") User user) {
    System.out.println(user);
  }
}
