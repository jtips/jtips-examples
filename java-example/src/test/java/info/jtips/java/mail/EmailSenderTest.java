package info.jtips.java.mail;

import jakarta.mail.MessagingException;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

class EmailSenderTest {

    EmailSender emailSender = new EmailSender("localhost", 2025);
//  EmailSender emailSender = new EmailSender(
//      "ns0.ovh.net", 587,
//      "mail@sewatech.fr", "QcI3VyTWsOVftjv5HoqH");

  @Test @Disabled("No SMTP server")
  void send_should_work() throws MessagingException {
    Email email = Email.builder()
        .from("mail@sewatech.fr")
        .to("alexis.hassler@gmail.com")
        .subject("Should work again")
        .content("This email should be sent.")
        .build();

    emailSender.send(email);
  }
}
