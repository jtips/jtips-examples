package info.jtips.java.time.json;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.ZonedDateTime;
import java.time.temporal.Temporal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class TemporalDeserializerTest {

  private static final ObjectMapper mapper = new ObjectMapper();

  @BeforeAll
  static void beforeAll() {
    JavaTimeModule javaTimeModule = new JavaTimeModule();
    javaTimeModule.addDeserializer(Temporal.class, TemporalDeserializer.INSTANCE);
    mapper.registerModule(javaTimeModule);
    mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
  }

  @Test
  void should_accept() throws JsonProcessingException {
    assertThat(mapper.readValue("123.456", Temporal.class))
        .isInstanceOf(Instant.class);
    assertThat(mapper.readValue("-123.456", Temporal.class))
        .isInstanceOf(Instant.class);

    assertThat(mapper.readValue("\"1970-01-01T01:23:45Z\"", Temporal.class))
        .isInstanceOf(ZonedDateTime.class);

    assertThat(mapper.readValue("\"1970-01-01T01:23:45\"", Temporal.class))
        .isInstanceOf(LocalDateTime.class);
    assertThat(mapper.readValue("\"1970-01-01\"", Temporal.class))
        .isInstanceOf(LocalDate.class);
    assertThat(mapper.readValue("\"01:23:45\"", Temporal.class))
        .isInstanceOf(LocalTime.class);

    assertThat(mapper.readValue("\"1970-01-01T01:23:45+02:00\"", Temporal.class))
        .isInstanceOf(OffsetDateTime.class);
    assertThat(mapper.readValue("\"1970-01-01T01:23:45-02:00\"", Temporal.class))
        .isInstanceOf(OffsetDateTime.class);
    assertThat(mapper.readValue("\"01:23:45+02:00\"", Temporal.class))
        .isInstanceOf(OffsetTime.class);
  }

  @Test
  void should_reject() {
    assertThatThrownBy(() -> mapper.readValue("1.2.3", Temporal.class))
        .isInstanceOf(JsonParseException.class);
    assertThatThrownBy(() -> mapper.readValue("\"1970-01-01T01:23:45W\"", Temporal.class))
        .isInstanceOf(InvalidFormatException.class);
    assertThatThrownBy(() -> mapper.readValue("\"1970-01-01X01:23:45\"", Temporal.class))
        .isInstanceOf(InvalidFormatException.class);
    assertThatThrownBy(() -> mapper.readValue("\"1970-01-1\"", Temporal.class))
        .isInstanceOf(InvalidFormatException.class);
    assertThatThrownBy(() -> mapper.readValue("\"970-01-01\"", Temporal.class))
        .isInstanceOf(InvalidFormatException.class);
    assertThatThrownBy(() -> mapper.readValue("\"1:23:45\"", Temporal.class))
        .isInstanceOf(InvalidFormatException.class);
    assertThatThrownBy(() -> mapper.readValue("\"1970-01-01T01:23:45+2000\"", Temporal.class))
        .isInstanceOf(InvalidFormatException.class);
    assertThatThrownBy(() -> mapper.readValue("\"01:23:45x02:00\"", Temporal.class))
        .isInstanceOf(InvalidFormatException.class);
  }
}
