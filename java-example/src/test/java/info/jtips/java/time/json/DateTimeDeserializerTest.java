package info.jtips.java.time.json;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class DateTimeDeserializerTest {

  private static final ObjectMapper mapper = new ObjectMapper();

  @BeforeAll
  static void beforeAll() {
    JavaTimeModule javaTimeModule = new JavaTimeModule();
    mapper.registerModule(javaTimeModule);
    mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
  }

  @Test
  void should_accept() throws JsonProcessingException {
    assertThat(mapper.readValue("123.456", Instant.class))
        .isAfter(Instant.EPOCH);
    assertThat(mapper.readValue("-123.456", Instant.class))
        .isBefore(Instant.EPOCH);
    assertThat(mapper.readValue("\"1970-01-01T01:23:45Z\"", Instant.class))
        .isAfter(Instant.EPOCH);

    assertThat(mapper.readValue("\"1970-01-01T01:23:45\"", LocalDateTime.class))
        .isAfter(LocalDateTime.ofEpochSecond(0, 0, ZoneOffset.UTC));
    assertThat(mapper.readValue("\"1970-01-01\"", LocalDate.class))
        .isEqualTo(LocalDate.EPOCH);
    assertThat(mapper.readValue("\"01:23:45\"", LocalTime.class))
        .isAfter(LocalTime.MIN);

    assertThat(mapper.readValue("\"1970-01-01T01:23:45+02:00\"", OffsetDateTime.class))
        .isBefore(OffsetDateTime.ofInstant(Instant.EPOCH, ZoneOffset.UTC));
    assertThat(mapper.readValue("\"1970-01-01T01:23:45-02:00\"", OffsetDateTime.class))
        .isAfter(OffsetDateTime.ofInstant(Instant.EPOCH, ZoneOffset.UTC));
    assertThat(mapper.readValue("\"1970-01-01T01:23:45+0200\"", OffsetDateTime.class))
        .isBefore(OffsetDateTime.ofInstant(Instant.EPOCH, ZoneOffset.UTC));
    assertThat(mapper.readValue("\"01:23:45+02:00\"", OffsetTime.class))
        .isBefore(OffsetTime.ofInstant(Instant.EPOCH, ZoneOffset.UTC));

    assertThat(mapper.readValue("\"1970-01-01T01:23:45+02:00\"", ZonedDateTime.class))
        .isBefore(ZonedDateTime.ofInstant(Instant.EPOCH, ZoneOffset.UTC));
    assertThat(mapper.readValue("\"1970-01-01T01:23:45-02:00\"", ZonedDateTime.class))
        .isAfter(ZonedDateTime.ofInstant(Instant.EPOCH, ZoneOffset.UTC));
    assertThat(mapper.readValue("\"1970-01-01T01:23:45+0200\"", ZonedDateTime.class))
        .isBefore(ZonedDateTime.ofInstant(Instant.EPOCH, ZoneOffset.UTC));
  }

  @Test
  void should_reject() {
    assertThatThrownBy(() -> mapper.readValue("1.2.3", Instant.class))
        .isInstanceOf(JsonParseException.class);
    assertThatThrownBy(() -> mapper.readValue("\"1970-01-01T01:23:45W\"", Instant.class))
        .isInstanceOf(InvalidFormatException.class);
    assertThatThrownBy(() -> mapper.readValue("\"1970-01-01X01:23:45\"", LocalDateTime.class))
        .isInstanceOf(InvalidFormatException.class);
    assertThatThrownBy(() -> mapper.readValue("\"1970-01-1\"", LocalDate.class))
        .isInstanceOf(InvalidFormatException.class);
    assertThatThrownBy(() -> mapper.readValue("\"970-01-01\"", LocalDate.class))
        .isInstanceOf(InvalidFormatException.class);
    assertThatThrownBy(() -> mapper.readValue("\"1:23:45\"", LocalTime.class))
        .isInstanceOf(InvalidFormatException.class);
    assertThatThrownBy(() -> mapper.readValue("\"1970-01-01T01:23:45+2000\"", OffsetDateTime.class))
        .isInstanceOf(InvalidFormatException.class);
    assertThatThrownBy(() -> mapper.readValue("\"01:23:45x02:00\"", OffsetTime.class))
        .isInstanceOf(InvalidFormatException.class);
  }
}
