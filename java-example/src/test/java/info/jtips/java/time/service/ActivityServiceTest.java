package info.jtips.java.time.service;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.TimeZone;

import static java.time.temporal.ChronoUnit.HOURS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ActivityServiceTest {

  @BeforeAll
  static void beforeAll() {
    TimeZone.setDefault(TimeZone.getTimeZone(ZoneOffset.UTC));
  }

  @Test
  void chooseActivityWithBuilder_should_sleep_in_the_morning() {
    // GIVeN
    Action action = mock(Action.class);
    DateBuilder dateBuilder = mock(DateBuilder.class);
    when(dateBuilder.currentLocalDateTime())
        .thenReturn(LocalDateTime.parse("1970-01-01T09:00"));
    ActivityService activityService = new ActivityService(
        action,
        dateBuilder,
        Clock.systemUTC()
    );

    // WHeN
    activityService.chooseActivityWithBuilder();

    // THeN
    verify(action, times(1)).doSleep();
    verify(action, never()).doPlay();
  }

  @Test
  void chooseActivityWithBuilder_should_play_in_the_afternoon() {
    // GIVeN
    Action action = mock(Action.class);
    DateBuilder dateBuilder = mock(DateBuilder.class);
    when(dateBuilder.currentLocalDateTime())
        .thenReturn(LocalDateTime.parse("1970-01-01T21:00"));
    ActivityService activityService = new ActivityService(
        action,
        dateBuilder,
        Clock.systemUTC()
    );

    // WHeN
    activityService.chooseActivityWithBuilder();

    // THeN
    verify(action, never()).doSleep();
    verify(action, times(1)).doPlay();
  }

  @Test
  void chooseActivityWithClock_should_sleep_in_the_morning() {
    // GIVeN
    Action action = mock(Action.class);
    DateBuilder dateBuilder = mock(DateBuilder.class);
    ActivityService activityService = new ActivityService(
        action,
        dateBuilder,
        Clock.fixed(Instant.EPOCH.plus(9, HOURS), ZoneOffset.UTC)
    );

    // WHeN
    activityService.chooseActivityWithClock();

    // THeN
    verify(action, times(1)).doSleep();
    verify(action, never()).doPlay();
  }

  @Test
  void chooseActivityWithClock_should_play_in_the_afternoon() {
    // GIVeN
    Action action = mock(Action.class);
    DateBuilder dateBuilder = mock(DateBuilder.class);
    Clock clock = Clock.fixed(Instant.EPOCH.plus(21, HOURS), ZoneOffset.UTC);
    ActivityService activityService = new ActivityService(
        action,
        dateBuilder,
        clock
    );

    // WHeN
    activityService.chooseActivityWithClock();

    // THeN
    verify(action, never()).doSleep();
    verify(action, times(1)).doPlay();
  }

  @Test
  void chooseActivityWithClock_should_play_in_the_afternoon___mock_version() {
    // GIVeN
    Action action = mock(Action.class);
    DateBuilder dateBuilder = mock(DateBuilder.class);
    Clock clock = mock(Clock.class);
    when(clock.getZone()).thenReturn(ZoneOffset.UTC);
    when(clock.instant()).thenReturn(Instant.EPOCH.plus(21, HOURS));
    ActivityService activityService = new ActivityService(
        action,
        dateBuilder,
        clock
    );
    System.out.println(Instant.now(clock));

    // WHeN
    activityService.chooseActivityWithClock();

    // THeN
    verify(action, never()).doSleep();
    verify(action, times(1)).doPlay();
  }
}
