package info.jtips.spring;

import info.jtips.spring.container.TestContainersInitializer;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.testcontainers.junit.jupiter.Testcontainers;

@Testcontainers
@SpringBootTest
@ContextConfiguration(initializers = TestContainersInitializer.class)
class SpringExampleApplicationTests {

	@Test
	void contextLoads() {
	}

}
