package info.jtips.spring.service;

import info.jtips.spring.container.TestContainersInitializer;
import info.jtips.spring.model.Product;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@Testcontainers
@SpringBootTest
@ContextConfiguration(initializers = TestContainersInitializer.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ProductServiceBootTest {

  @Autowired
  ProductService service;

  @Test
  void getAll_should_work() {
    // GIVeN

    // WHeN
    List<Product> products = service.getAll();

    // THeN
    assertThat(products).isEmpty();
  }
}
