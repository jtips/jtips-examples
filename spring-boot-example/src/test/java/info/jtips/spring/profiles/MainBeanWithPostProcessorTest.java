package info.jtips.spring.profiles;

import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles(profiles = "test", resolver = EnhancedActiveProfileResolver.class)
class MainBeanWithPostProcessorTest extends MainBeanTest {

  MainBeanWithPostProcessorTest() {
    this.expectedProfiles = new String[] {"test", "processor"};
  }

}
