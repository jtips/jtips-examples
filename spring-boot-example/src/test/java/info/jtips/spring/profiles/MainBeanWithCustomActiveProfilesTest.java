package info.jtips.spring.profiles;

@CustomSpringBootTest
class MainBeanWithCustomActiveProfilesTest extends MainBeanTest {

  MainBeanWithCustomActiveProfilesTest() {
    this.expectedProfiles = new String[] {"test", "processor"};
  }

}
