package info.jtips.spring.profiles;

import info.jtips.spring.container.TestContainersInitializer;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.testcontainers.junit.jupiter.Testcontainers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowableOfType;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@Testcontainers
@SpringBootTest
@ContextConfiguration(initializers = TestContainersInitializer.class)
class MainBeanTest {

  String[] expectedProfiles;

  @Autowired
  MainBean mainBean;

  MainBeanTest() {
    this.expectedProfiles = new String[] {"test", "cloud", "processor"};
  }

  @Test
  void getActiveProfiles_should_work() {
    // GIVeN

    // WHeN
    String[] profiles = mainBean.getActiveProfiles();

    // THeN
    assertThat(profiles)
            .containsExactlyInAnyOrder(this.expectedProfiles);
  }

  @Test
  void someBean_should_be_TestOnlyBean() {
    // GIVeN

    // WHeN
    Class<? extends SomeBean> type = mainBean.getSomeBeanType();

    // THeN
    assertThat(type)
            .isEqualTo(TestOnlyBean.class);
  }

}

