package info.jtips.spring.profiles;

import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles(profiles = "test", resolver = EnhancedActiveProfileResolver.class)
class MainBeanWithEnhancedResolverTest extends MainBeanTest {

  MainBeanWithEnhancedResolverTest() {
    this.expectedProfiles = new String[] {"test", "processor"};
  }

}
