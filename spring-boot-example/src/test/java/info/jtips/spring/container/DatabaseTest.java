package info.jtips.spring.container;

import com.github.dockerjava.api.model.ExposedPort;
import com.github.dockerjava.api.model.PortBinding;
import com.github.dockerjava.api.model.Ports;
import com.zaxxer.hikari.HikariDataSource;
import info.jtips.spring.dao.ProductDao;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.transaction.BeforeTransaction;
import org.testcontainers.containers.PostgreSQLContainer;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@TestPropertySource("classpath:application-test.properties")
public class DatabaseTest {

  PostgreSQLContainer<?> pg;

  @Value("${spring.datasource.url}")
  String datasourceUrl;
  @Value("${spring.datasource.username}")
  String datasourceUsername;
  @Value("${spring.datasource.password}")
  String datasourcePassword;

  @Autowired
  ProductDao productDao;

  @BeforeEach
  public void init() {
    // jdbc:postgresql://[server]:[port]/[database]?[key]=[value]
    Pattern pattern = Pattern.compile(".*://(.*):(.*)/([^?]*)?.*");
    Matcher urlMatcher = pattern.matcher(datasourceUrl);
    if (urlMatcher.matches()) {
      pg = new PostgreSQLContainer<>("postgres:14")
          .withDatabaseName(urlMatcher.group(3))
          .withUsername(datasourceUsername)
          .withPassword(datasourcePassword)
          .withCreateContainerCmdModifier(
              e -> e.getHostConfig()
                  .withPortBindings(
                      new PortBinding(
                          Ports.Binding.parse(urlMatcher.group(2)),
                          new ExposedPort(5432))
                  )
          )
          .withInitScript("init-db.sql");
      pg.start();
    }
  }

  @AfterEach
  public void clean() {
    pg.stop();
  }

  @Test
  void containers_should_be_running() {
    assertThat(pg.isRunning()).isTrue();
  }

  @Test
  void should_be_able_to_connect_to_pg() {
    productDao.findAll();
  }
}
