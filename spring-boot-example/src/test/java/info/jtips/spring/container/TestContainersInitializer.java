package info.jtips.spring.container;

import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.PostgreSQLContainer;

import java.util.Map;

public class TestContainersInitializer
    implements ApplicationContextInitializer<ConfigurableApplicationContext> {

  static final int SMTP_PORT = 1025;
  static final int HTTP_PORT = 8025;

  static final PostgreSQLContainer<?> pg = new PostgreSQLContainer<>("postgres:14")
      .withDatabaseName("viseo3")
      .withUsername("chambersign_admin")
      .withPassword("secret")
      .withInitScript("init-db.sql");
  static final GenericContainer<?> smtp = new GenericContainer<>("druidfi/mailhog")
      .withExposedPorts(SMTP_PORT, HTTP_PORT);

  @Override
  public void initialize(ConfigurableApplicationContext context) {
    if (!pg.isRunning()) {
      pg.start();
    }
    if (!smtp.isRunning()) {
      smtp.start();
    }

    TestPropertyValues
        .of(Map.of(
            "spring.datasource.url", pg.getJdbcUrl(),
            "spring.datasource.username", pg.getUsername(),
            "spring.datasource.password", pg.getPassword(),
            "spring.mail.host", smtp.getHost(),
            "spring.mail.port", smtp.getMappedPort(SMTP_PORT).toString(),
            "spring.mail.http-port", smtp.getMappedPort(HTTP_PORT).toString()
        ))
        .applyTo(context);
  }

}
