package info.jtips.spring.container;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.test.context.ContextConfiguration;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.junit.jupiter.Testcontainers;

import static org.assertj.core.api.Assertions.assertThat;

@Testcontainers
@SpringBootTest
@ContextConfiguration(initializers = TestContainersInitializer.class)
public class SmtpTest {

  @Autowired
  JavaMailSender emailSender;

  @Test
  void should_be_able_to_send_mail() {
    SimpleMailMessage message = new SimpleMailMessage();
    message.setFrom("author@jtips.info");
    message.setTo("reader@jtips.info");
    message.setSubject("Should work");
    message.setText("This email should be sent.");

    emailSender.send(message);
  }

}
