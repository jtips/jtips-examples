package info.jtips.spring.container;

import info.jtips.spring.dao.ProductDao;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.test.context.ContextConfiguration;
import org.testcontainers.junit.jupiter.Testcontainers;

import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;

@Testcontainers
@SpringBootTest
@ContextConfiguration(initializers = TestContainersInitializer.class)
public class ContainerTest {

  @Autowired
  ProductDao productDao;

  @Autowired
  JavaMailSender mailSender;

  @Test
  void should_be_able_to_connect_to_pg() {
    productDao.findAll();
  }

  @Test
  void should_be_able_to_send_text_email() {
    // GIVeN
    SimpleMailMessage message = new SimpleMailMessage();
    message.setFrom("author@jtips.info");
    message.setTo("reader@jtips.info");
    message.setCc("copy@jtips.info");
    message.setBcc("blind@jtips.info");
    message.setReplyTo("nothing@jtips.info");
    message.setSubject("Hello World");
    message.setText("Bla bla bla");

    // WHeN
    mailSender.send(message);

    // THeN
  }

  @Test
  void should_be_able_to_send_html_email() throws MessagingException {
    // GIVeN
    MimeMessage message = mailSender.createMimeMessage();
    MimeMessageHelper helper = new MimeMessageHelper(message, "utf-8");
    helper.setFrom("author@jtips.info");
    helper.setTo("reader@jtips.info");
    helper.setCc("copy@jtips.info");
    helper.setBcc("blind@jtips.info");
    helper.setReplyTo("nothing@jtips.info");
    helper.setSubject("Hello World");
    helper.setText("<p>Bla bla bla</p>", true);

    // WHeN
    mailSender.send(message);

    // THeN
  }
}
