package info.jtips.container;

import dasniko.testcontainers.keycloak.KeycloakContainer;
import info.jtips.container.auth.Token;
import io.restassured.RestAssured;
import io.restassured.config.SSLConfig;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.representations.idm.ClientRepresentation;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.List;

@Testcontainers
public class KeycloakAuthTest {

  private static final String REALM_NAME = "sewatech";
  private static final String CLIENT_ID = "jtips";
  private static final String USERNAME = "jtips";
  private static final String PASSWORD = "jtipspwd";
  @Container
  static KeycloakContainer keycloak = new KeycloakContainer()
      .useTls()
      .withRealmImportFile("realm-export.json");

  static String clientSecret;

  @BeforeAll
  static void beforeAll() {
    Keycloak admin = keycloak.getKeycloakAdminClient();

    UserRepresentation user = new UserRepresentation();
    user.setEnabled(Boolean.TRUE);
    user.setUsername(USERNAME);
    CredentialRepresentation credential = new CredentialRepresentation();
    credential.setValue(PASSWORD);
    user.setCredentials(List.of(credential));
    admin.realm(REALM_NAME).users().create(user);

    List<ClientRepresentation> clients = admin.realm(REALM_NAME).clients().findByClientId(CLIENT_ID);
    clientSecret = admin.realm(REALM_NAME)
        .clients().get(clients.get(0).getId())
        .generateNewSecret().getValue();

    RestAssured.config = RestAssured.config().sslConfig(
        SSLConfig.sslConfig().trustStore("tls.jks", "changeit")
    );
  }

  @Test
  void should_work_with_local_kc() {
    ValidatableResponse response =
        RestAssured.given()
            .baseUri(keycloak.getAuthServerUrl())
            .contentType(ContentType.URLENC)
            .formParam("username", USERNAME)
            .formParam("password", PASSWORD)
            .formParam("grant_type", "password")
            .formParam("client_id", CLIENT_ID)
            .formParam("client_secret", clientSecret)
            .when()
            .post("/realms/%s/protocol/openid-connect/token".formatted(REALM_NAME))
            .then()
            .statusCode(200);

    Token token = response.extract().as(Token.class);
    Assertions.assertThat(token).isNotNull();
    System.out.println(token);
  }

}
