package info.jtips.container.auth;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Token {
  @JsonProperty("access_token")
  private String accessToken;
  @JsonProperty("expires_in")
  private String expiresIn;
  @JsonProperty("refresh_token")
  private String refreshToken;
  @JsonProperty("refresh_expires_in")
  private String refreshExpiresIn;
  @JsonProperty("token_type")
  private String tokenType;

  public String getAccessToken() {
    return accessToken;
  }

  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }

  @Override
  public String toString() {
    return "Token{" +
        "accessToken='" + accessToken + '\'' +
        '}';
  }
}
