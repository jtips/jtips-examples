package info.jtips.spring.profiles;

import info.jtips.spring.configuration.SewatechConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import jakarta.annotation.PostConstruct;

@Component
public class MainBean {

  private final SewatechConfig sewatechConfig;
  private final Environment environment;
  private final SomeBean someBean;

  public MainBean(SewatechConfig sewatechConfig, Environment environment, SomeBean someBean) {
    this.sewatechConfig = sewatechConfig;
    this.environment = environment;
    this.someBean = someBean;
  }

  @PostConstruct
  public void init() {
    System.out.println("Default Profiles:  " + String.join(", ", environment.getDefaultProfiles()));
    System.out.println("Active Profiles:   " + String.join(", ", environment.getActiveProfiles()));
    System.out.println("SomeBean instance: " + someBean.getClass().getName());
    System.out.println("Config version   : " + sewatechConfig.getVersion());
  }

  public String[] getActiveProfiles() {
    return environment.getActiveProfiles();
  }

  public Class<? extends SomeBean> getSomeBeanType() {
    return someBean.getClass();
  }
}
