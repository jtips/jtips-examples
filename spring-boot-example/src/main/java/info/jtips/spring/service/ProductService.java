package info.jtips.spring.service;

import info.jtips.spring.dao.ProductDao;
import info.jtips.spring.misc.Logging;
import info.jtips.spring.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ProductService {

  @Autowired
  private ProductDao dao;

  public List<Product> getAll() {
    return dao.findAll();
  }

  @Logging
  public Product create(Product product) {
    return dao.create(product);
  }

  public void removeAll() {
    dao.removeAll();
  }

}
