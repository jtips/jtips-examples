package info.jtips.spring.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PostPersist;
import jakarta.persistence.PostUpdate;

@Configurable
public class ProductPersistenceListener {

    @Autowired
    private EntityManager entityManager;

    private LazyInjector injector;

    @PostPersist @PostUpdate
    public void afterSave(Product product) {
        inject();
        System.out.println("After save " + product);
    }

    private void inject() {
        if (injector == null) {
            injector = new LazyInjector(this);
        }
    }

}
