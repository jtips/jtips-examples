package info.jtips.spring.model;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.AnnotationBeanWiringInfoResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.beans.factory.wiring.BeanConfigurerSupport;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jakarta.annotation.PostConstruct;

@Configurable
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class LazyInjector {
    @Autowired
    private BeanFactory beanFactory;
    private Object bean;

    public LazyInjector(Object bean) {
        this.bean = bean;
    }

    @PostConstruct
    public void init() {
        BeanConfigurerSupport beanConfigurerSupport = new BeanConfigurerSupport();
        beanConfigurerSupport.setBeanWiringInfoResolver(new AnnotationBeanWiringInfoResolver());
        beanConfigurerSupport.setBeanFactory(this.beanFactory);
        beanConfigurerSupport.configureBean(bean);
    }
}
