package info.jtips.spring.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("sewatech")
public class SewatechConfig {

  private String version;
  private String[] url;

  public String getVersion() {
    return version;
  }
  public void setVersion(String version) {
    this.version = version;
  }

  public String[] getUrl() {
    return url;
  }

  public void setUrl(String[] url) {
    this.url = url;
  }
}
