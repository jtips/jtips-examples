package info.jtips.spring.dao;

import info.jtips.spring.model.Product;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.event.service.spi.EventListenerGroup;
import org.hibernate.event.service.spi.EventListenerRegistry;
import org.hibernate.event.spi.PersistEventListener;
import org.hibernate.internal.FastSessionServices;
import org.hibernate.service.spi.ServiceRegistryImplementor;
import org.springframework.stereotype.Repository;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.PersistenceUnit;
import java.util.List;
import java.util.Optional;

@Repository
public class ProductDao {

  @PersistenceContext
  private EntityManager em;

  @PersistenceUnit
  private EntityManagerFactory emf;

  public List<Product> findAll() {
    SessionFactoryImplementor sf = emf.unwrap(SessionFactoryImplementor.class);
    FastSessionServices fastSessionServices = sf.getFastSessionServices();
    EventListenerGroup<PersistEventListener> eventListenerGroup_persist = fastSessionServices.eventListenerGroup_PERSIST;

    ServiceRegistryImplementor sr = sf.getServiceRegistry();
    EventListenerRegistry eventListenerRegistry = sr.getService( EventListenerRegistry.class );

    return em.createQuery(
            "select p from Product as p",
            Product.class)
        .getResultList();
  }

  public Optional<Product> findById(Long id) {
    return Optional.ofNullable(em.find(Product.class, id));
  }

  public List<Product> findByTitleLike(String title) {
    return em.createQuery(
            "select p from Product as p where p.title like :title",
            Product.class)
        .setParameter("title", title + '%')
        .getResultList();
  }

  public Product update(Product product) {
    return product;
  }

  public Product create(Product product) {
    em.persist(product);
    return product;
  }

  public void remove(Product product) {
    em.remove(em.find(Product.class, product.getId()));
  }

  public void removeAll() {
    em.createQuery("delete from Product").executeUpdate();
  }

}
