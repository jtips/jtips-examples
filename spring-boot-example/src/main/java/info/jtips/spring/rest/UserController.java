package info.jtips.spring.rest;

import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {

  @GetMapping("/self")
  public String getSelfUser(JwtAuthenticationToken auth) {
    return auth.getTokenAttributes().get("preferred_username").toString();
  }

}
