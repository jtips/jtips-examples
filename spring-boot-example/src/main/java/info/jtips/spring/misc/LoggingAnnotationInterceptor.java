package info.jtips.spring.misc;

import info.jtips.spring.model.Product;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class LoggingAnnotationInterceptor {

  @Before("logging()")
  public void logBefore(JoinPoint point) {
    System.out.println("==> Début : " + point.getSignature());
  }

  @AfterThrowing(pointcut = "logging()", throwing = "exception")
  public void logAfterThrowing(JoinPoint point, Exception exception) {
    System.out.printf("==> Problème : %.100s sur '%s'%n", exception, point.getSignature());
  }

  @AfterReturning(pointcut = "logging()", returning = "result")
  public void logAfterReturning(JoinPoint point, Product result) {
    System.out.printf("==> Résultat : %.100s de '%s'%n", result, point.getSignature());
  }

  @Pointcut("@annotation(info.jtips.spring.misc.Logging) || @within(info.jtips.spring.misc.Logging)")
  private void logging() {
  }

}
