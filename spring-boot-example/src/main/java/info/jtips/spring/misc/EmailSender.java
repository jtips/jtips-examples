package info.jtips.spring.misc;

import jakarta.mail.Message;
import jakarta.mail.MessagingException;
import jakarta.mail.Session;
import jakarta.mail.Transport;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeMessage;
import java.util.Properties;

public class EmailSender {

  private final Session session;

  public EmailSender(String hostname, int port) {
    Properties properties = System.getProperties();
    properties.setProperty("mail.smtp.host", hostname);
    properties.setProperty("mail.smtp.port", String.valueOf(port));
    session = Session.getDefaultInstance(properties);
  }

  public void send(Email email) throws MessagingException {
    MimeMessage message = new MimeMessage(session);
    message.setFrom(new InternetAddress(email.from));
    message.addRecipient(Message.RecipientType.TO, new InternetAddress(email.to));
    message.setSubject(email.subject);
    message.setText(email.content);
    Transport.send(message);
    System.out.println("Sent message successfully....");
  }

}
