package info.jtips.spring.misc;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.AnnotationBeanWiringInfoResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.wiring.BeanConfigurerSupport;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class ConfigurableInterceptor {

    @Autowired
    private BeanFactory beanFactory;
    private final BeanConfigurerSupport beanConfigurerSupport = new BeanConfigurerSupport();

    public ConfigurableInterceptor(BeanFactory beanFactory) {
        this.beanConfigurerSupport.setBeanWiringInfoResolver(new AnnotationBeanWiringInfoResolver());
        this.beanFactory = beanFactory;
        this.beanConfigurerSupport.setBeanFactory(this.beanFactory);
    }

    @AfterReturning(pointcut = "configurable()")
    public void afterReturning(JoinPoint point) {
        System.out.println("---");
    }

//    @Pointcut("@annotation(info.jtips.spring.misc.Logging) || @within(info.jtips.spring.misc.Logging)")
    @Pointcut("@annotation(org.springframework.beans.factory.annotation.Configurable) || @within(org.springframework.beans.factory.annotation.Configurable)")
    private void configurable() {
    }
}
