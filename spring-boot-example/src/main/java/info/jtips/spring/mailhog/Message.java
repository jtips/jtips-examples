package info.jtips.spring.mailhog;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Message {
  @JsonProperty("ID")
  private String id;
  @JsonProperty("Content")
  private Content content;
  @JsonProperty("Created")
  private String created;

  // Probably useless
  @JsonProperty("From")
  private Address from;
  @JsonProperty("To")
  private Address[] to;
  @JsonProperty("MIME")
  private String mime;
  @JsonProperty("Raw")
  private Raw raw;

  public String getId() {
    return id;
  }

  public String getFrom() {
    return raw.getFrom();
  }

  public String[] getTo() {
    return content.getHeader("To");
  }

  public String[] getCc() {
    return content.getHeader("Cc");
  }

  public String getReplyTo() {
    return content.getSimpleHeader("Reply-To");
  }

  public String getSubject() {
    return content.getSimpleHeader("Subject");
  }

  public String getBody() {
    return content.getBody();
  }

  public String getContentType() {
    return content.getSimpleHeader("Content-Type");
  }
}
