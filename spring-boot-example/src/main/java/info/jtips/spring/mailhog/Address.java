package info.jtips.spring.mailhog;

import com.fasterxml.jackson.annotation.JsonProperty;

class Address {
  @JsonProperty("Relays")
  private String relays;
  @JsonProperty("Mailbox")
  private String mailbox;
  @JsonProperty("Domain")
  private String domain;
  @JsonProperty("Params")
  private String params;
}
