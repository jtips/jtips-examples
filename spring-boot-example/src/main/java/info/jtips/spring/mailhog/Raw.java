package info.jtips.spring.mailhog;

import com.fasterxml.jackson.annotation.JsonProperty;

class Raw {
  @JsonProperty("From")
  private String from;
  @JsonProperty("To")
  private String[] to;
  @JsonProperty("Data")
  private String data;
  @JsonProperty("Helo")
  private String helo;

  String getFrom() {
    return from;
  }

  String[] getTo() {
    return to;
  }
}
