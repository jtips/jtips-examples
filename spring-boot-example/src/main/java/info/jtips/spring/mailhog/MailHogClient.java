package info.jtips.spring.mailhog;

import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;

import jakarta.annotation.PostConstruct;
import java.net.URI;
import java.util.Collections;
import java.util.List;

@Component
public class MailHogClient {

  private final RestTemplate restTemplate = new RestTemplate(new SimpleClientHttpRequestFactory());
  private final Environment environment;

  private String baseUrl;

  public MailHogClient(Environment environment) {
    this.environment = environment;
  }

  @PostConstruct
  public void init() {
    // Regular converter supports "application/json" but MailHog provides "text/json"
    MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
    converter.setSupportedMediaTypes(List.of(new MediaType("text", "json")));
    restTemplate.setMessageConverters(List.of(converter));

    baseUrl = String.format("http://%s:%s/api",
            environment.getProperty("spring.mail.host"),
            environment.getProperty("spring.mail.http-port")
        );
  }

  public Page findMessages() {
    URI uri = new DefaultUriBuilderFactory(baseUrl).builder()
        .pathSegment("v2", "messages")
        .build();
    return get(uri, Page.class);
  }

  public Message findMessageByID(String id) {
    URI uri = new DefaultUriBuilderFactory(this.baseUrl).builder()
        .pathSegment("v1", "messages", id)
        .build();
    return get(uri, Message.class);
  }

  public void deleteAllMessages() {
    URI uri = new DefaultUriBuilderFactory(this.baseUrl).builder()
        .pathSegment("v1", "messages")
        .build();
    restTemplate.delete(uri);
  }

  public void deleteMessageByID(String id) {
    URI uri = new DefaultUriBuilderFactory(this.baseUrl).builder()
        .pathSegment("v1", "messages", id)
        .build();
    restTemplate.delete(uri);
  }

  // PRIVATE
  private <E> E get(URI uri, Class<E> type) {
    HttpHeaders headers = new HttpHeaders();
    headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
    return restTemplate.exchange(uri, HttpMethod.GET, new HttpEntity<Void>(headers), type).getBody();
  }

}
