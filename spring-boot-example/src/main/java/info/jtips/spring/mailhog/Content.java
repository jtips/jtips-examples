package info.jtips.spring.mailhog;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

class Content {
  @JsonProperty("Headers")
  private Map<String, String[]> headers;
  @JsonProperty("Body")
  private String body;
  @JsonProperty("Size")
  private String size;
  @JsonProperty("MIME")
  private String mime;

  public String[] getHeader(String name) {
    return headers.get(name);
  }
  public String getSimpleHeader(String name) {
    return headers.get(name)[0];
  }

  public String getBody() {
    return body;
  }

  public String getSize() {
    return size;
  }

  public String getMime() {
    return mime;
  }
}
