package info.jtips.spring.mailhog;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Page {
  @JsonProperty("total")
  private int total;
  @JsonProperty("count")
  private int count;
  @JsonProperty("start")
  private int start;
  @JsonProperty("items")
  private Message[] items;

  public int getTotal() {
    return total;
  }

  public int getCount() {
    return count;
  }

  public int getStart() {
    return start;
  }

  public Message[] getItems() {
    return items;
  }
}
