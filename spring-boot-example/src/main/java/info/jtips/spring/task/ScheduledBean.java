package info.jtips.spring.task;

import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.concurrent.TimeUnit;

@Component
@Profile("scheduling")
public class ScheduledBean {

  private final ScheduleLogger logger = new ScheduleLogger(ScheduledBean.class);

  @Scheduled(fixedDelay = 10, timeUnit = TimeUnit.SECONDS)
  private void scheduledRate() {
    logger.logAndWait("rate ", 1_234);
  }

  @Scheduled(fixedRate = 10, timeUnit = TimeUnit.SECONDS)
  private void scheduledDelay() {
    logger.logAndWait("delay", 1_234);
  }

  @Scheduled(cron = "*/10 * * * * *", zone = "UTC")
  private void scheduledCron() {
    logger.logAndWait("cron ", 1_234);
  }
}
