package info.jtips.spring.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Component;

import jakarta.annotation.PostConstruct;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;

import static java.time.temporal.ChronoUnit.SECONDS;

@Component
@Profile("scheduling")
public class ScheduledDynamicBean {

  private final TaskScheduler taskScheduler;
  private final ScheduleLogger logger = new ScheduleLogger(ScheduledDynamicBean.class);

  public ScheduledDynamicBean(@Autowired(required = false) TaskScheduler taskScheduler) {
    this.taskScheduler = taskScheduler;
  }

  @PostConstruct
  private void init() {
    if (taskScheduler == null) {
      return;
    }

    taskScheduler.scheduleWithFixedDelay(
        () -> logger.logAndWait("delay    ", 1_234),
        Duration.of(10, SECONDS)
    );

    taskScheduler.scheduleAtFixedRate(
        () -> logger.logAndWait("rate     ", 1_234),
        Duration.of(10, SECONDS)
    );

    taskScheduler.schedule(
        () -> logger.logAndWait("cron     ", 1_234),
        new CronTrigger("*/10 * * * * *")
    );

    scheduleIn10s();

    // Custom trigger, same result as fixed delay
    taskScheduler.schedule(
        () -> logger.logAndWait("trigger ", 1_234),
        triggerContext -> Instant.now().plus(10, SECONDS)
    );
  }

  private void scheduleIn10s() {
    taskScheduler.schedule(
        () -> {
          scheduleIn10s();
          logger.logAndWait("manual ", 1_234);
        },
        Instant.now().plus(10, SECONDS)
    );
  }

}
