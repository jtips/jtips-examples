package info.jtips.spring.task;

import info.jtips.spring.dao.ProductDao;
import info.jtips.spring.model.Product;
import info.jtips.spring.service.ProductService;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.concurrent.TimeUnit;

@Component
@Profile("scheduling")
public class ProductScheduledBean {

  private final ScheduleLogger logger = new ScheduleLogger(ProductScheduledBean.class);
  private final ProductDao productDao;
  private final ProductService productService;

  public ProductScheduledBean(ProductDao productDao, ProductService productService) {
    this.productDao = productDao;
    this.productService = productService;
  }

  @Scheduled(fixedDelay = 1, timeUnit = TimeUnit.MINUTES)
  private void createViaService() {
    Product product = new Product();
    this.productService.create(product);
    logger.logAndWait(this.productService.getAll().size() + " products", 1);
  }

  @Scheduled(fixedDelay = 10, timeUnit = TimeUnit.MINUTES, initialDelay = 5)
  private void cleanViaService() {
    this.productService.removeAll();
    logger.logAndWait(this.productService.getAll().size() + " products", 1);
  }

  @Scheduled(fixedDelay = 1, timeUnit = TimeUnit.HOURS)
  @Transactional
  private void createViaDao() {
    Product product = new Product();
    this.productDao.create(product);
    logger.logAndWait(this.productDao.findAll().size() + " products", 1);
  }

}
