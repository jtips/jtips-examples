package info.jtips.spring.task;

import java.time.Instant;

public class ScheduleLogger {

  private final Class<?> clazz;

  public ScheduleLogger(Class<?> clazz) {
    this.clazz = clazz;
  }

  public void logAndWait(String type, int delay) {
    System.out.printf(
        "%2$s [%4$s] %3$s - Scheduled %1$s%n",
        type, Instant.now(), clazz.getSimpleName(), Thread.currentThread().getName());
    try {
      Thread.sleep(delay);
    } catch (InterruptedException e) {
      Thread.currentThread().interrupt();
    }
  }

}
