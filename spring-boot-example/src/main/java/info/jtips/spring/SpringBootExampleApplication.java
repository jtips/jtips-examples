package info.jtips.spring;

import info.jtips.spring.profiles.CommonProfileInitializer;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.task.TaskSchedulerCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.EnableLoadTimeWeaving;
import org.springframework.context.annotation.aspectj.EnableSpringConfigured;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication(exclude = org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class)
@EnableWebMvc
@EnableScheduling
@EnableSpringConfigured
//@EnableAspectJAutoProxy
public class SpringBootExampleApplication {

  public static void main(String[] args) {
    new SpringApplicationBuilder()
        .sources(SpringBootExampleApplication.class)
        .initializers(new CommonProfileInitializer())
        .profiles("dev")
        .build()
        .run(args);
  }

  @Bean
  public TaskSchedulerCustomizer taskSchedulerCustomizer() {
    return taskScheduler -> taskScheduler.setPoolSize(Runtime.getRuntime().availableProcessors());
  }
}
