package info.jtips.spring.profiles;

import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

@ActiveProfiles(profiles = "test")
@ContextConfiguration(initializers = CommonProfileInitializer.class)
class MainBeanWithInitializerTest extends MainBeanTest {

  MainBeanWithInitializerTest() {
    this.expectedProfiles = new String[] {"test", "common"};
  }

}
