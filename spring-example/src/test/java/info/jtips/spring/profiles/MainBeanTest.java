package info.jtips.spring.profiles;

import info.jtips.spring.config.ApplicationConfiguration;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@WebAppConfiguration
@ContextConfiguration(classes = ApplicationConfiguration.class)
class MainBeanTest {

  String[] expectedProfiles;

  @Autowired
  MainBean mainBean;

  MainBeanTest() {
    this.expectedProfiles = new String[] {"test"};
  }

  @Test
  void getActiveProfiles_should_work() {
    // GIVeN

    // WHeN
    String[] profiles = mainBean.getActiveProfiles();

    // THeN
    assertThat(profiles)
        .containsExactlyInAnyOrder(this.expectedProfiles);
  }

  @Test
  void someBean_should_be_TestOnlyBean() {
    // GIVeN

    // WHeN
    Class<? extends SomeBean> type = mainBean.getSomeBeanType();

    // THeN
    assertThat(type)
        .isEqualTo(TestOnlyBean.class);
  }

}
