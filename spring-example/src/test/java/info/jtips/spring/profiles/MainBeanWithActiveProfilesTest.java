package info.jtips.spring.profiles;

import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles(profiles = "test")
class MainBeanWithActiveProfilesTest extends MainBeanTest {

  MainBeanWithActiveProfilesTest() {
    this.expectedProfiles = new String[] {"test"};
  }

}
