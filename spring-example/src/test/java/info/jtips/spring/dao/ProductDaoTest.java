package info.jtips.spring.dao;

import info.jtips.spring.config.ApplicationConfiguration;
import info.jtips.spring.model.Product;
import info.jtips.spring.model.ProductBuilder;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@WebAppConfiguration
@ContextConfiguration(classes = ApplicationConfiguration.class)
@Transactional
class ProductDaoTest {

  @Autowired
  ProductDao productDao;

  Product product;

  @BeforeEach
  void init() {
    product = productDao.create(
        new ProductBuilder()
            .setTitle("TEST-TITLE")
            .setAuthor("TEST-AUTHOR")
            .setPrice(999.99)
            .build()
    );
  }

  @AfterEach
  void clear() {
    productDao.remove(product);
  }

  @Test
  void findByTitle_should_work() {
    // GIVeN

    // WHeN
    List<Product> products = productDao.findByTitleLike("TEST");

    // THeN
    assertThat(products)
        .hasSize(1)
        .extracting(Product::getTitle)
        .allMatch(title -> title.startsWith("TEST"), "start with TEST");
  }
}
