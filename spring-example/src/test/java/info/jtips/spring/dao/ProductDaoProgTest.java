package info.jtips.spring.dao;

import info.jtips.spring.model.Product;
import info.jtips.spring.model.ProductBuilder;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;

import java.util.List;
import java.util.function.Supplier;

import static org.assertj.core.api.Assertions.assertThat;

@TestInstance(Lifecycle.PER_CLASS)
class ProductDaoProgTest {

  PlatformTransactionManager transactionManager;
  ProductDao productDao;

  Product product;

  @BeforeAll
  void initAll() {
    ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("application-config.xml");
    transactionManager = context.getBean(PlatformTransactionManager.class);
    productDao = context.getBean(ProductDao.class);
  }

  @BeforeEach
  void init() {
    TransactionStatus initStatus = transactionManager.getTransaction(null);
    product = productDao.create(
        new ProductBuilder()
            .setTitle("TEST-TITLE")
            .setAuthor("TEST-AUTHOR")
            .setPrice(999.99)
            .build()
    );
    transactionManager.commit(initStatus);
  }

  @AfterEach
  void clean() {
    TransactionStatus cleanStatus = transactionManager.getTransaction(null);
    productDao.remove(product);
    transactionManager.commit(cleanStatus);
  }

  @Test
  void findByTitle_should_work() {
    // GIVeN

    // WHeN
    List<Product> products = runInTransaction(() -> productDao.findByTitleLike("TEST"));

    // THeN
    assertThat(products)
        .hasSize(1)
        .extracting(Product::getTitle)
        .allMatch(title -> title.startsWith("TEST"), "start with TEST");
  }

  <T> T runInTransaction(Supplier<T> supplier) {
    TransactionStatus status = transactionManager.getTransaction(null);
    try {
      T result = supplier.get();
      transactionManager.commit(status);
      return result;
    } catch (Exception e) {
      transactionManager.rollback(status);
      throw e;
    }
  }
}
