package info.jtips.spring.misc;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.expression.BeanFactoryResolver;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.security.access.expression.SecurityExpressionRoot;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;

class SpelTest {

    @Test
    void expression_simple() {
        String exp = "0.1 + T(java.lang.Math).random()";

        ExpressionParser parser = new SpelExpressionParser();
        Expression expression = parser.parseExpression(exp);
        Double value = expression.getValue(Double.class);

        assertThat(value).isPositive();
        logResult(exp, value);
    }

    @Test
    void expression_with_param() {
        String exp = "new java.util.Random().nextInt(#bound)";

        ExpressionParser parser = new SpelExpressionParser();
        Expression expression = parser.parseExpression(exp);

        StandardEvaluationContext context = new StandardEvaluationContext();
        context.setVariable("bound", 100);

        Integer value = expression.getValue(context, Integer.class);

        assertThat(value)
                .isGreaterThanOrEqualTo(-100)
                .isLessThanOrEqualTo(100);
        logResult(exp, value);
    }

    @Test
    void expression_with_bean() {
        ConfigurableListableBeanFactory beanFactory = initBeanFactory();

        String exp = "@randomService.nextInt(#bound)";

        ExpressionParser parser = new SpelExpressionParser();
        Expression expression = parser.parseExpression(exp);

        StandardEvaluationContext context = new StandardEvaluationContext();
        context.setVariable("bound", 100);
        context.setBeanResolver(new BeanFactoryResolver(beanFactory));

        Integer value = expression.getValue(context, Integer.class);

        assertThat(value)
                .isGreaterThanOrEqualTo(-100)
                .isLessThanOrEqualTo(100);
        logResult(exp, value);
    }

    @Test
    void expression_with_security() {
        initBeanFactory();

        String exp = "hasRole('ROLE_ADMIN')";

        ExpressionParser parser = new SpelExpressionParser();
        Expression expression = parser.parseExpression(exp);

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        SecurityExpressionRoot securityRoot = new SecurityExpressionRoot(authentication) {};

        Boolean value = expression.getValue(securityRoot, Boolean.class);

        assertThat(value).isTrue();
        logResult(exp, value);
    }


    private static void logResult(String exp, Object value) {
        System.out.printf("Expression '%s' => %s%n", exp, value);
    }

    private static ConfigurableListableBeanFactory initBeanFactory() {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();
        applicationContext.registerBean("randomService", RandomService.class);

        Authentication authentication = new TestingAuthenticationToken("Administrator", null, "ROLE_ADMIN");
        SecurityContextHolder.getContext().setAuthentication(authentication);

        return applicationContext.getBeanFactory();
    }

    public static class RandomService {
        public int nextInt(int bound) {
            return new Random().nextInt(bound);
        }
    }

}
