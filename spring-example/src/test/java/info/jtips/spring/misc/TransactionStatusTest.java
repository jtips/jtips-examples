package info.jtips.spring.misc;

import info.jtips.spring.config.ApplicationConfiguration;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.transaction.BeforeTransaction;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@WebAppConfiguration
@ContextConfiguration(classes = ApplicationConfiguration.class)
@Transactional
class TransactionStatusTest {

  @BeforeAll
  static void init_all() {
    assertThat(TransactionSynchronizationManager.isActualTransactionActive())
        .isFalse();
  }

  @BeforeTransaction
  void init_before_transaction() {
    assertThat(TransactionSynchronizationManager.isActualTransactionActive())
        .isFalse();
  }

  @BeforeEach
  void init_within_transaction() {
    assertThat(TransactionSynchronizationManager.isActualTransactionActive())
        .isTrue();
  }

  @Test
  void transactional_should_be_active() {
    assertThat(TransactionSynchronizationManager.isActualTransactionActive())
        .isTrue();
  }

}
