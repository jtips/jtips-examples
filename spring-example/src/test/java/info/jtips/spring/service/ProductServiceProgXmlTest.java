package info.jtips.spring.service;

import info.jtips.spring.model.Product;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ProductServiceProgXmlTest {

  ProductService service;

  @BeforeAll
  void init() {
    ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("application-config.xml");
    service = context.getBean(ProductService.class);
  }

  @Test
  void getAll_should_work() {
    // GIVeN

    // WHeN
    List<Product> products = service.getAll();

    // THeN
    assertThat(products).isEmpty();
  }

  @Test
  void create_should_work() {
    // GIVeN

    // WHeN
    service.create(Product.builder().build());

    // THeN
  }

}
