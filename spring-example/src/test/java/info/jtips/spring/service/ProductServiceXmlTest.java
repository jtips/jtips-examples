package info.jtips.spring.service;

import info.jtips.spring.model.Product;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(locations={"/application-config.xml"})
class ProductServiceXmlTest {

  @Autowired
  ProductService service;

  @Test
  void getAll_should_work() {
    // GIVeN

    // WHeN
    List<Product> products = service.getAll();

    // THeN
    assertThat(products).isEmpty();
  }
}
