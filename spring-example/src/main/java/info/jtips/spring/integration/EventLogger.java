package info.jtips.spring.integration;

import info.jtips.java.logging.LoggerConfiguration;

import static java.lang.System.Logger.Level.INFO;

public class EventLogger {

  private final System.Logger systemLogger;

  public EventLogger() {
    LoggerConfiguration.configuration(EventLogger.class.getSimpleName())
        .skipLevel();
    this.systemLogger = System.getLogger(EventLogger.class.getSimpleName());
  }

  public void log(String action, String address, Object message) {
    this.log(String.format("%s %s => %s", action, address, message));
  }

  public void log(String action, String address) {
    this.log(String.format("%s %s", action, address));
  }

  public void log(String message) {
    systemLogger.log(INFO, message);
    try {
      Thread.sleep(10);
    } catch (InterruptedException e) {
      Thread.currentThread().interrupt();
    }
  }

}
