package info.jtips.spring.integration;

import info.jtips.spring.misc.SecurityService;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.ChannelInterceptor;
import org.springframework.stereotype.Component;

@Component
public class SecurityInterceptor implements ChannelInterceptor {

  private final SecurityService securityService;

  public SecurityInterceptor(SecurityService securityService) {
    this.securityService = securityService;
  }

  @Override
  public Message<?> preSend(Message<?> message, MessageChannel channel) {
    String token = message.getHeaders().get("token", String.class);
    securityService.validateToken(token);
    if (channel instanceof SecuredChannel) {
      securityService.checkAuthorized(token, ((SecuredChannel) channel).getPermissionsRequired());
    }
    return ChannelInterceptor.super.preSend(message, channel);
  }
}
