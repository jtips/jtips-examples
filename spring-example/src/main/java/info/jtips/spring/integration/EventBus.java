package info.jtips.spring.integration;

import org.springframework.integration.IntegrationMessageHeaderAccessor;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.channel.ExecutorChannel;
import org.springframework.integration.channel.PublishSubscribeChannel;
import org.springframework.integration.channel.QueueChannel;
import org.springframework.integration.channel.RendezvousChannel;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.PollableChannel;
import org.springframework.messaging.SubscribableChannel;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Function;

@Component
public class EventBus {

  private static final ExecutorService THREAD_POOL = Executors.newFixedThreadPool(10);

  private static final Function<String, SubscribableChannel> PUBSUB_BUILDER =
//      key -> new ExecutorSubscribableChannel(THREAD_POOL);
      key -> new PublishSubscribeChannel(THREAD_POOL, false);
  private final ConcurrentMap<String, SubscribableChannel> pubSubChannels = new ConcurrentHashMap<>();

  private static final Function<String, PollableChannel> P2P_BUILDER =
      key -> new QueueChannel();
  private final ConcurrentMap<String, PollableChannel> p2pPollChannels = new ConcurrentHashMap<>();

  private static final Function<String, SubscribableChannel> P2PSUB_BUILDER =
      key -> new DirectChannel();
  private final ConcurrentMap<String, SubscribableChannel> p2pSubChannels = new ConcurrentHashMap<>();

  private static final Function<String, SubscribableChannel> P2PREQ_BUILDER =
      key -> new ExecutorChannel(THREAD_POOL);
  private final ConcurrentMap<String, SubscribableChannel> p2pReqChannels = new ConcurrentHashMap<>();

  private final EventLogger logger = new EventLogger();

  /**
   * Publish in a pub/sub channel
   */
  public EventBus publish(String address, Object content) {
    logger.log("Publishing to topic", address, content);
    pubSubChannels.computeIfAbsent(address, PUBSUB_BUILDER)
        .send(new GenericMessage<>(content));
    return this;
  }

  /**
   * Send in a p2p channel
   */
  public EventBus send2Poll(String address, Object content) {
    logger.log("Sending to queue", address, content);
    p2pPollChannels.computeIfAbsent(address, P2P_BUILDER)
        .send(new GenericMessage<>(content, Map.of(IntegrationMessageHeaderAccessor.PRIORITY, 1)));
    return this;
  }

  /**
   * Send in a p2p channel (with subscribers)
   */
  public EventBus send2Sub(String address, Object content) {
    logger.log("Sending", address, content);
    p2pSubChannels.computeIfAbsent(address, P2PSUB_BUILDER)
        .send(new GenericMessage<>(content));
    return this;
  }

  public Message<?> consume(String address) {
    logger.log("Consuming queue", address);
    PollableChannel channel = p2pPollChannels.get(address);
    return channel != null ? channel.receive() : null;
  }

  public void registerTopic(String address, MessageHandler handler) {
    System.out.printf("[%s] Registering topic %s%n", Thread.currentThread().getName(), address);
    pubSubChannels.computeIfAbsent(address, PUBSUB_BUILDER)
        .subscribe(handler);
  }

  public void unregisterTopic(String address, MessageHandler handler) {
    pubSubChannels.computeIfAbsent(address, PUBSUB_BUILDER)
        .unsubscribe(handler);
  }

  public void registerQueue(String address, MessageHandler handler) {
    p2pSubChannels.computeIfAbsent(address, PUBSUB_BUILDER)
        .subscribe(handler);
  }

  public void unregisterQueue(String address, MessageHandler handler) {
    logger.log("Requesting", address);
    p2pSubChannels.computeIfAbsent(address, PUBSUB_BUILDER)
        .unsubscribe(handler);
  }

  // Request / reply

  public Object request(String address, Object content) {
    logger.log("Sending request", address);
    PollableChannel replyChannel = new RendezvousChannel();
    Message<Object> message = new GenericMessage<>(content, Map.of(MessageHeaders.REPLY_CHANNEL, replyChannel));
    p2pReqChannels.computeIfAbsent(address, P2PREQ_BUILDER)
        .send(message);
    return replyChannel.receive().getPayload();
  }

  public void registerRequestQueue(String address) {
    logger.log("Registering request queue", address);
    p2pReqChannels.computeIfAbsent(address, P2PREQ_BUILDER)
        .subscribe(message -> {
          MessageChannel channel = (MessageChannel) message.getHeaders().getReplyChannel();
          logger.log("Replying", address);
          channel.send(new GenericMessage<>("REPLY"));
        });
  }

}
