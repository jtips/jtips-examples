package info.jtips.spring.integration;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.core.GenericMessagingTemplate;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.WebApplicationContext;

import static java.lang.System.Logger.Level.INFO;

@RestController
@Scope(WebApplicationContext.SCOPE_APPLICATION)
@RequestMapping("/channel")
public class StaticController {

  private final System.Logger systemLogger = System.getLogger(StaticController.class.getSimpleName());

  private final ApplicationContext applicationContext;
  private final EventPublisher eventPublisher;

  public StaticController(
      ApplicationContext applicationContext,
      EventPublisher eventPublisher) {
    this.applicationContext = applicationContext;
    this.eventPublisher = eventPublisher;
  }

  @PostMapping("/{name}")
  public Object publish(@PathVariable String name, @RequestBody String content) {
    GenericMessagingTemplate messagingTemplate = new GenericMessagingTemplate();
    MessageChannel channel = applicationContext.getBean("channel/" + name, MessageChannel.class);
    messagingTemplate.setDefaultDestination(channel);
    return messagingTemplate.sendAndReceive(new GenericMessage<>(content))
        .getPayload();
  }

  @PostMapping("/request")
  public Object request(@RequestBody String content) {
    GenericMessagingTemplate messagingTemplate = new GenericMessagingTemplate();
    MessageChannel channel = applicationContext.getBean("channel/request", MessageChannel.class);
    messagingTemplate.setDefaultDestination(channel);
    return messagingTemplate.sendAndReceive(new GenericMessage<>(content))
        .getPayload();
  }

  @PostMapping("/publish")
  public void publish(@RequestBody String content) {
    String publishedContent = eventPublisher.publish(content, "xxxxxxx");
    systemLogger.log(INFO, "Message sent on channel/publish: " + publishedContent);
  }

}
