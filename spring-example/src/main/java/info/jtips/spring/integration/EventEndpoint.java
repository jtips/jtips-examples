package info.jtips.spring.integration;

import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.Poller;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;

import static java.lang.System.Logger.Level.INFO;

@MessageEndpoint
public class EventEndpoint {

  private final System.Logger systemLogger = System.getLogger(EventEndpoint.class.getSimpleName());

  @ServiceActivator(
      inputChannel = "channel/pollable",
      outputChannel = "channel/subscribable")
  public String onPollable(String message) {
    systemLogger.log(INFO, "Message received on channel/pollable: " + message);
    return "Reply-" + message;
  }

  @ServiceActivator(
      inputChannel = "channel/pollable",
      poller = @Poller("sw.slowPoller"))
  public void onPollableSlow(String message) {
    systemLogger.log(INFO, "Message received on channel/pollable (slow): " + message);
  }

  @ServiceActivator(
      inputChannel = "channel/pollable",
      poller = @Poller(fixedDelay = "1000"))
  public void onPollableInternal(String message) {
    systemLogger.log(INFO, "Message received on channel/pollable (internal): " + message);
  }

  @ServiceActivator(inputChannel = "channel/subscribable")
  public void onSubscribable(String message) {
    systemLogger.log(INFO, "Message received on channel/subscribable: " + message);
  }

  @ServiceActivator(
      inputChannel = "channel/request")
  public String onRequest(Message<String> message) {
    systemLogger.log(INFO, "Message received on channel/request: " + message.getPayload());
    return "Reply-" + message.getPayload();
  }

//  @ServiceActivator(inputChannel = "channel/publish")
//  public void onPublish(Message<String> message) {
//    systemLogger.log(INFO, "Message received on channel/publish: %s with token %s", message.getPayload(), message.getHeaders().get("token"));
//  }
  @ServiceActivator(inputChannel = "channel/publish")
  public void onPublish(String message, @Header("token") String token) {
    systemLogger.log(INFO, "Message received on channel/publish: %s with token %s", message, token);
  }

}
