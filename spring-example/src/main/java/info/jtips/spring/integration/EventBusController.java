package info.jtips.spring.integration;

import info.jtips.java.concurrent.MessageBus;
import org.springframework.context.annotation.Scope;
import org.springframework.integration.support.channel.HeaderChannelRegistry;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.WebApplicationContext;

import java.util.concurrent.atomic.AtomicInteger;

@RestController
@Scope(WebApplicationContext.SCOPE_APPLICATION)
@RequestMapping("/bus")
public class EventBusController {

  private final EventBus eventBus;
  private final AtomicInteger counter = new AtomicInteger();

  private final EventLogger eventLogger = new EventLogger();

  public EventBusController(EventBus eventBus) {
    this.eventBus = eventBus;
  }

  @PostMapping("/topic/{address}")
  public void publish(@PathVariable String address, @RequestBody String content) {
    eventBus.publish(address, content);
  }

  @PostMapping("/topic/subscription/{address}")
  public void registerTopic(@PathVariable String address) {
    int count = counter.incrementAndGet();
    eventBus.registerTopic(address, message -> eventLogger.log("On message - " + count, address, message.getPayload()));
  }

  @PostMapping("/queue-poll/{address}")
  public void send2Poll(@PathVariable String address, @RequestBody String content) {
    eventBus.send2Poll(address, content);
  }

  @GetMapping("/queue-poll/{address}")
  public Object consume(@PathVariable String address) {
    Message<?> message = eventBus.consume(address);
    this.eventLogger.log("Consuming", address, message == null ? "N/A" : message.getHeaders().get(MessageHeaders.TIMESTAMP));
    return message == null ? null : message.getPayload();
  }

  @PostMapping("/queue-sub/{address}")
  public void send2Subscribers(@PathVariable String address, @RequestBody String content) {
    eventBus.send2Sub(address, content);
  }

  @PostMapping("/queue-sub/subscription/{address}")
  public void registerQueue(@PathVariable String address) {
    int count = counter.incrementAndGet();
    eventBus.registerQueue(address, message -> eventLogger.log("On message - " + count, address, message.getPayload()));
  }

  @PostMapping("/request/{address}")
  public Object request(@PathVariable String address, @RequestBody String content) {
    return eventBus.request(address, content);
  }

  @PostMapping("/request/subscription/{address}")
  public void registerRequestQueue(@PathVariable String address) {
    eventBus.registerRequestQueue(address);
  }
}
