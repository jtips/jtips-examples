package info.jtips.spring.integration;

import java.util.Set;

public interface SecuredChannel {
  Set<String> getPermissionsRequired();
}
