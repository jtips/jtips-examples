package info.jtips.spring.integration;

import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.Publisher;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;

import static java.lang.System.Logger.Level.INFO;

@MessageEndpoint
public class EventPublisher {

  private final System.Logger systemLogger = System.getLogger(EventPublisher.class.getSimpleName());

  @Publisher("channel/publish")
  public String publish(String message, @Header("token") String token) {
    systemLogger.log(INFO, "Message sending on channel/publish: " + message);
    return "Reply-" + message;
  }

}
