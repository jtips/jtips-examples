package info.jtips.spring.model;

public class ProductBuilder {
  private Long id;
  private String title;
  private String author;
  private Double price;

  public ProductBuilder setId(Long id) {
    this.id = id;
    return this;
  }

  public ProductBuilder setTitle(String title) {
    this.title = title;
    return this;
  }

  public ProductBuilder setAuthor(String author) {
    this.author = author;
    return this;
  }

  public ProductBuilder setPrice(Double price) {
    this.price = price;
    return this;
  }

  public Product build() {
    return new Product(id, title, author, price);
  }
}
