package info.jtips.spring.config;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseFactory;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import jakarta.annotation.PostConstruct;
import javax.sql.DataSource;

@Configuration
@EnableWebMvc // Enable WebMvcConfigurationSupport, and adds message converters (jackson,...)
@EnableTransactionManagement
@EnableAspectJAutoProxy
@EnableScheduling
@ComponentScan("info.jtips.spring")
public class ApplicationConfiguration implements SchedulingConfigurer {

  @PostConstruct
  public void onStartup() {
    System.out.println("=== Annotation based configuration");
  }

  @Bean//(autowireCandidate = false)
  @Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
  public DataSource dataSource() {
    DriverManagerDataSource dataSource = new DriverManagerDataSource();
    dataSource.setDriverClassName("org.postgresql.Driver");
    dataSource.setUrl("jdbc:postgresql://localhost:6432/jtips");
    dataSource.setUsername("jtips");
    dataSource.setPassword("jtipspwd");

    return dataSource;
  }

  @Bean
  @Primary
  public DataSource embeddedDatabase() {
    EmbeddedDatabaseFactory factory = new EmbeddedDatabaseFactory();
    factory.setDatabaseType(EmbeddedDatabaseType.H2);
    return factory.getDatabase();
  }

  @Bean
  public LocalContainerEntityManagerFactoryBean emf() {
    LocalContainerEntityManagerFactoryBean emf = new LocalContainerEntityManagerFactoryBean();
    emf.setDataSource(dataSource());

    return emf;
  }

  @Bean
  public PlatformTransactionManager transactionManager() {
    return new JpaTransactionManager(emf().getObject());
  }

  @Bean
  public TaskScheduler taskScheduler() {
    ThreadPoolTaskScheduler poolTaskScheduler = new ThreadPoolTaskScheduler();
    poolTaskScheduler.setPoolSize(5);
    return poolTaskScheduler;
  }

  @Override
  public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
    taskRegistrar.setScheduler(taskScheduler());
  }
}
