package info.jtips.spring.config;

import info.jtips.spring.integration.SecurityInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.ExecutorChannel;
import org.springframework.integration.channel.QueueChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.config.EnablePublisher;
import org.springframework.integration.scheduling.PollerMetadata;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.PollableChannel;
import org.springframework.messaging.SubscribableChannel;
import org.springframework.scheduling.support.PeriodicTrigger;

import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Configuration
@EnableIntegration
@EnablePublisher
public class IntegrationConfiguration {

  @Bean("channel/pollable")
  public PollableChannel pollableChannel(SecurityInterceptor securityInterceptor) {
    QueueChannel channel = new QueueChannel();
    channel.addInterceptor(securityInterceptor);
    return channel;
  }

  @Bean("channel/subscribable")
  public SubscribableChannel subscribableChannel() {
    return new ExecutorChannel(Executors.newFixedThreadPool(4));
  }

  @Bean("channel/request")
  public PollableChannel requestChannel() {
    return new QueueChannel();
  }

  @Bean(name = PollerMetadata.DEFAULT_POLLER)
  public PollerMetadata defaultPoller() {
    PollerMetadata pollerMetadata = new PollerMetadata();
    pollerMetadata.setTrigger(new PeriodicTrigger(1, TimeUnit.SECONDS));
    return pollerMetadata;
  }

  @Bean(name = "sw.slowPoller")
  public PollerMetadata slowPoller() {
    PollerMetadata pollerMetadata = new PollerMetadata();
    pollerMetadata.setTrigger(new PeriodicTrigger(1, TimeUnit.MINUTES));
    return pollerMetadata;
  }
}
