package info.jtips.spring.profiles;

import org.springframework.stereotype.Component;

@Component
@TestProfile
public class TestOnlyBean implements SomeBean {
}
