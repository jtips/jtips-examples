package info.jtips.spring.profiles;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import jakarta.annotation.PostConstruct;

@Component
public class MainBean {

  private final Environment environment;
  private final SomeBean someBean;

  public MainBean(Environment environment, SomeBean someBean) {
    this.environment = environment;
    this.someBean = someBean;
  }

  @PostConstruct
  public void init() {
    System.out.println("Default Profiles:  " + String.join(", ", environment.getDefaultProfiles()));
    System.out.println("Active Profiles:   " + String.join(", ", environment.getActiveProfiles()));
    System.out.println("SomeBean instance: " + someBean.getClass().getName());
  }

  public String[] getActiveProfiles() {
    return environment.getActiveProfiles();
  }

  public Class<? extends SomeBean> getSomeBeanType() {
    return someBean.getClass();
  }
}
