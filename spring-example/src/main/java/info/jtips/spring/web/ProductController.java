package info.jtips.spring.web;

import info.jtips.spring.model.Product;
import info.jtips.spring.model.ProductBuilder;
import info.jtips.spring.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import java.net.URI;
import java.util.List;

import static org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder.*;

@RestController
@Scope(WebApplicationContext.SCOPE_APPLICATION)
@RequestMapping("/product")
public class ProductController {

  private final ProductService service;

  public ProductController(ProductService service) {
    this.service = service;
  }

  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<List<Product>> getAll() {
    return ResponseEntity.ok(service.getAll());
  }

  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE, path = "/{id}")
  public ResponseEntity<Product> getOne(@PathVariable("id") Long id) {
    return ResponseEntity.ok(Product.builder().setId(id).setTitle("Title").build());
  }

  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Void> post(@RequestBody Product product) {
    Product createdProduct = service.create(product);
    if (createdProduct.getId() == null) {
      createdProduct.setId(0L);
    }

    URI uri = fromMethodCall(
            on(ProductController.class).post(createdProduct)
        )
        .pathSegment(createdProduct.getId().toString())
        .build()
        .encode()
        .toUri();
    return ResponseEntity.created(uri).build();
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<Void> delete(@PathVariable Long id) {
    service.delete(id);
    return ResponseEntity.noContent().build();
  }

}
