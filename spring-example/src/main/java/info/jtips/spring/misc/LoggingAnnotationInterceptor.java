package info.jtips.spring.misc;

import info.jtips.spring.model.Product;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class LoggingAnnotationInterceptor {

  @Before("logging()")
  public void logBefore(JoinPoint point) {
    System.out.println("==> Début : " + point.getSignature());
  }

  @AfterThrowing(pointcut = "logging()", throwing = "exception")
  public void logAfterThrowing(JoinPoint point, Exception exception) {
    System.out.printf("==> Problème : %.100s sur '%s'%n", exception, point.getSignature());
  }

  @AfterReturning(pointcut = "logging()", returning = "result")
  public void logAfterReturning(JoinPoint point, Product result) {
    System.out.printf("==> Résultat : %.100s de '%s'%n", result, point.getSignature());
  }

  @Pointcut("@annotation(info.jtips.spring.config.Logging) || @within(info.jtips.spring.config.Logging)")
  private void logging() {
  }

}
