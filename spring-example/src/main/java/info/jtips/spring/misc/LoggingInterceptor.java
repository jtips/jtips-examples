package info.jtips.spring.misc;

import info.jtips.spring.model.Product;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.aop.aspectj.MethodInvocationProceedingJoinPoint;
import org.springframework.stereotype.Component;

import jakarta.annotation.PostConstruct;

//@Component
@Aspect
public class LoggingInterceptor {

  @Before("execution(* info.jtips.spring.dao.*.find*(..))")
  public void logDaoFind(JoinPoint point) {
    System.out.println("==> Début méthode dao.find : " + point.getSignature());
  }

  @AfterThrowing(pointcut = "execution(* info.jtips.spring.service.*.*(..))", throwing = "exception")
  public void logServiceProblem(JoinPoint point, Exception exception) {
    System.out.printf("==> Problème : %.100s sur '%s'%n", exception, point.getSignature());
  }

  @AfterReturning(pointcut = "execution(* info.jtips.spring.service.*.*(..))", returning = "result")
  public void logServiceReturn(JoinPoint point, Product result) {
    System.out.printf("==> Résultat : %.100s de '%s'%n", result, point.getSignature());
  }

  @Around("service()")
  public Object logService(ProceedingJoinPoint point) throws Throwable {
    String methodName = point.getTarget().getClass().getSimpleName() + "." + point.getSignature().getName();
    System.out.println("==> Début méthode de service : " + methodName);
    Object obj = point.proceed();
    System.out.println("<== Fin méthode de service : " + methodName);

    return obj;
  }

  @Before("get(* info.jtips.spring.model.Product.*)")
  public void logNotService(JoinPoint point) {
    System.out.println("XXXX " + point.getSignature());
  }

  @Pointcut("execution(* info.jtips.spring.service.*.*(..))")
  private void service() {
  }

}
