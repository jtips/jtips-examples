package info.jtips.spring.misc;

import org.springframework.stereotype.Component;

import java.util.Set;

public interface SecurityService {

  void validateToken(String token);

  void checkAuthorized(String token, Set<String> permissionsRequired);

  @Component
  class OkSecurityImpl implements SecurityService {
    @Override
    public void validateToken(String token) {
      return;
    }

    @Override
    public void checkAuthorized(String token, Set<String> permissionsRequired) {
      return;
    }
  }
}
