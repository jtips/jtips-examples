package info.jtips.spring.task;

import java.time.Instant;

public class ScheduleLogger {

  private final Class<?> clazz;

  public ScheduleLogger(Class<?> clazz) {
    this.clazz = clazz;
  }

  public void logAndWait(String type, int delay) {
//    System.out.printf("%s - Scheduled %s : %s [%s]%n", clazz, type, Instant.now(), Thread.currentThread().getName());
    try {
      Thread.sleep(delay);
    } catch (InterruptedException e) {
      Thread.currentThread().interrupt();
    }
  }

}
