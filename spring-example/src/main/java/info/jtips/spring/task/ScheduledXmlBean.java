package info.jtips.spring.task;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

public class ScheduledXmlBean {

  private final ScheduleLogger logger = new ScheduleLogger(ScheduledXmlBean.class);

  public void scheduledRate() {
    logger.logAndWait("rate   ", 1_234);
  }

  public void scheduledDelay() {
    logger.logAndWait("delay  ", 1_234);
  }

  public void scheduledCron() {
    logger.logAndWait("cron   ", 1_234);
  }

  public void scheduledTrigger() {
    logger.logAndWait("trigger", 1_234);
  }
}
