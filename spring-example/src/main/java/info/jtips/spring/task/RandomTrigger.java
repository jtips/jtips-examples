package info.jtips.spring.task;

import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.TriggerContext;

import java.security.SecureRandom;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;

public class RandomTrigger implements Trigger {

  @Override
  public Instant nextExecution(TriggerContext triggerContext) {
    Instant initialInstant = triggerContext.lastActualExecution() == null
            ? Instant.now()
            : triggerContext.lastActualExecution();
    return initialInstant.plus(new SecureRandom().nextInt(20), ChronoUnit.SECONDS);
  }

}
