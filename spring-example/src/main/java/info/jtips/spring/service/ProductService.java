package info.jtips.spring.service;

import info.jtips.spring.config.Logging;
import info.jtips.spring.dao.ProductDao;
import info.jtips.spring.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import java.util.List;

@TransactionalService
public class ProductService {

  @Autowired
  private ProductDao dao;

  public List<Product> getAll() {
    System.out.println("Read only tx: " + TransactionSynchronizationManager.isCurrentTransactionReadOnly());
    return dao.findAll();
  }

  @Logging
  @Transactional
  public Product create(Product product) {
    System.out.println("Read only tx: " + TransactionSynchronizationManager.isCurrentTransactionReadOnly());
    return dao.create(product);
  }

  @Transactional
  public Product delete(Long id) {
    throw new UnsupportedOperationException();
  }
}
