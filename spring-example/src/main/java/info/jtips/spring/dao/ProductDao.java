package info.jtips.spring.dao;

import info.jtips.spring.model.Product;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional(propagation= Propagation.MANDATORY)
public class ProductDao {

  @PersistenceContext
  private EntityManager em;

  @Transactional(readOnly=true)
  public List<Product> findAll() {
    return em.createQuery(
            "select p from Product as p",
            Product.class)
        .getResultList();
  }

  @Transactional(readOnly=true)
  public Optional<Product> findById(Long id) {
    return Optional.ofNullable(em.find(Product.class, id));
  }

  public List<Product> findByTitleLike(String title) {
    return em.createQuery(
            "select p from Product as p where p.title like :title",
            Product.class)
        .setParameter("title", title + '%')
        .getResultList();
  }

  public Product update(Product product) {
    return product;
  }

  public Product create(Product product) {
    em.persist(product);
    return product;
  }

  public void remove(Product product) {
    em.remove(em.find(Product.class, product.getId()));
  }

}
