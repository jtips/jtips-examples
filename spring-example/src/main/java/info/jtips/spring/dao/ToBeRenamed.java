package info.jtips.spring.dao;

import info.jtips.spring.model.Product;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.ManagedType;
import javax.persistence.metamodel.Metamodel;
import java.util.Set;

@Component
public class ToBeRenamed {

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    public void init() {
        Metamodel metamodel = entityManager.getMetamodel();
        EntityType<Product> productType = metamodel.entity(Product.class);
        String name = productType.getName();
        Set<ManagedType<?>> managedTypes = metamodel.getManagedTypes();
    }
}
