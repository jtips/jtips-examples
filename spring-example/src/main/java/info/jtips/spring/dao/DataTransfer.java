package info.jtips.spring.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;

@Component
public class DataTransfer {

  @Autowired
  private DataSource dataSource;

  @Autowired @Qualifier("embeddedDatabase")
  private DataSource embeddedDb;

}
