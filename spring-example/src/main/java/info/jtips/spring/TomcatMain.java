package info.jtips.spring;

import info.jtips.spring.config.ApplicationConfiguration;
import org.apache.catalina.Context;
import org.apache.catalina.Wrapper;
import org.apache.catalina.startup.Tomcat;
import org.apache.tomcat.util.scan.StandardJarScanner;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.context.support.XmlWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import java.io.File;
import java.io.IOException;
import java.util.Locale;

public class TomcatMain {

  public static void main(String[] args) throws Exception {
    ConfigurationMode mode;
    if (args.length == 0) {
      mode = ConfigurationMode.ANNOTATION;
    } else {
      mode = ConfigurationMode.fromName(args[0]);
    }

    Tomcat tomcat = new Tomcat();
    tomcat.setBaseDir(createTempDir(tomcat.getConnector().getPort()));

    String appBase = ".";
    tomcat.getHost().setAppBase(appBase);
    Context context = tomcat.addWebapp("", appBase);
    ((StandardJarScanner) context.getJarScanner()).setScanManifest(false);

    initSpring(context, mode);

    tomcat.getConnector();
    tomcat.start();
    tomcat.getServer().await();
  }

  private static void initSpring(Context context, ConfigurationMode mode) {
    // Load Spring web application configuration
    WebApplicationContext applicationContext = buildApplicationContext(mode);

    // Create and register the DispatcherServlet
    Wrapper servlet = Tomcat.addServlet(context, "spring", new DispatcherServlet(applicationContext));
    servlet.setLoadOnStartup(1);
    context.addServletMappingDecoded("/api/*", "spring");
  }

  private static WebApplicationContext buildApplicationContext(ConfigurationMode mode) {
    switch (mode) {
      case ANNOTATION:
        AnnotationConfigWebApplicationContext annotationApplicationContext = new AnnotationConfigWebApplicationContext();
        annotationApplicationContext.register(ApplicationConfiguration.class);
        return annotationApplicationContext;
      case XML:
        XmlWebApplicationContext xmlApplicationContext = new XmlWebApplicationContext();
        xmlApplicationContext.setConfigLocation("classpath:application-config.xml");
        return xmlApplicationContext;
      default:
        return null;
    }
  }

  // based on AbstractEmbeddedServletContainerFactory
  private static String createTempDir(int port) {
    try {
      File tempDir = File.createTempFile("tomcat.", "." + port);
      tempDir.delete();
      tempDir.mkdir();
      tempDir.deleteOnExit();
      return tempDir.getAbsolutePath();
    } catch (IOException ex) {
      throw new RuntimeException(
          "Unable to create tempDir. java.io.tmpdir is set to " + System.getProperty("java.io.tmpdir"),
          ex
      );
    }
  }

  enum ConfigurationMode {
    ANNOTATION, XML;

    static ConfigurationMode fromName(String name) {
      if (name == null) {
        return ANNOTATION;
      } else {
        return ConfigurationMode.valueOf(name.toUpperCase(Locale.ROOT));
      }
    }
  }

}
